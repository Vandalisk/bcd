--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: talking_stick_rooms_before_delete_row_tr(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION talking_stick_rooms_before_delete_row_tr() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    UPDATE calls SET ended_at = now(),
                     duration = age(OLD.last_used::timestamp, started_at::timestamp)
                     WHERE calls.room_id = OLD.id;
    RETURN OLD;
END;
$$;


--
-- Name: tasks_after_update_of_is_completed_row_tr(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION tasks_after_update_of_is_completed_row_tr() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    IF NEW.is_completed THEN
      UPDATE users SET current_score = current_score + NEW.points FROM treatments
      WHERE treatments.receiver_id = users.id AND treatments.id = NEW.treatment_id;
    ELSE
      UPDATE users SET current_score = current_score - NEW.points FROM treatments
      WHERE treatments.receiver_id = users.id AND treatments.id = NEW.treatment_id;
    END IF;
    RETURN NULL;
END;
$$;


--
-- Name: treatments_after_update_row_tr(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION treatments_after_update_row_tr() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    IF OLD.status = 'active' AND NEW.status <> 'active' THEN
      UPDATE users SET total_score = total_score - NEW.scores,
                       current_score = current_score - round(NEW.scores *
                          cast((SELECT COUNT(*) FROM tasks WHERE tasks.treatment_id = NEW.id AND tasks.is_completed = true) as float)
                          /
                          (SELECT COUNT(*) FROM tasks WHERE tasks.treatment_id = NEW.id)
                       )
                       WHERE users.id = NEW.receiver_id;
    ELSIF NEW.status = 'active' AND OLD.status <>'active' THEN
      UPDATE users SET total_score = total_score + NEW.scores WHERE users.id = NEW.receiver_id;
    END IF;
    RETURN NULL;
END;
$$;


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: calls; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE calls (
    id integer NOT NULL,
    room_id integer,
    started_at timestamp without time zone,
    ended_at timestamp without time zone,
    caller_id integer,
    recipient_id integer,
    duration character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: calls_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE calls_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: calls_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE calls_id_seq OWNED BY calls.id;


--
-- Name: feed_events; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE feed_events (
    id integer NOT NULL,
    resource_type character varying,
    resource_id integer,
    sender_id bigint,
    receiver_id bigint,
    data text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: feed_events_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE feed_events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: feed_events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE feed_events_id_seq OWNED BY feed_events.id;


--
-- Name: measures; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE measures (
    id integer NOT NULL,
    value numeric NOT NULL,
    "when" character varying,
    date date NOT NULL,
    patient_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: measures_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE measures_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: measures_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE measures_id_seq OWNED BY measures.id;


--
-- Name: messages; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE messages (
    id integer NOT NULL,
    sender_id bigint DEFAULT 0,
    receiver_id bigint DEFAULT 0,
    text text,
    is_last boolean DEFAULT false NOT NULL,
    is_read boolean DEFAULT false NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    message_type character varying DEFAULT 'common'::character varying
);


--
-- Name: messages_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE messages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: messages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE messages_id_seq OWNED BY messages.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


--
-- Name: sync_exports; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE sync_exports (
    id integer NOT NULL,
    options text,
    total_count integer DEFAULT 0,
    done_count integer DEFAULT 0,
    last_id integer DEFAULT 0,
    status character varying,
    user_id bigint,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: sync_exports_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE sync_exports_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sync_exports_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE sync_exports_id_seq OWNED BY sync_exports.id;


--
-- Name: talking_stick_participants; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE talking_stick_participants (
    id integer NOT NULL,
    name character varying,
    ip character varying,
    guid character varying,
    joined_at timestamp without time zone,
    last_seen timestamp without time zone,
    room_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    connected boolean
);


--
-- Name: talking_stick_participants_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE talking_stick_participants_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: talking_stick_participants_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE talking_stick_participants_id_seq OWNED BY talking_stick_participants.id;


--
-- Name: talking_stick_rooms; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE talking_stick_rooms (
    id integer NOT NULL,
    name character varying,
    last_used timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    slug character varying
);


--
-- Name: talking_stick_rooms_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE talking_stick_rooms_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: talking_stick_rooms_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE talking_stick_rooms_id_seq OWNED BY talking_stick_rooms.id;


--
-- Name: talking_stick_signals; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE talking_stick_signals (
    id integer NOT NULL,
    room_id integer,
    sender_id integer,
    recipient_id integer,
    signal_type character varying,
    data text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: talking_stick_signals_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE talking_stick_signals_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: talking_stick_signals_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE talking_stick_signals_id_seq OWNED BY talking_stick_signals.id;


--
-- Name: tasks; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE tasks (
    id integer NOT NULL,
    treatment_id integer,
    date date,
    is_completed boolean DEFAULT false NOT NULL,
    data_completed text,
    comment text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    points integer DEFAULT 0
);


--
-- Name: tasks_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE tasks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tasks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE tasks_id_seq OWNED BY tasks.id;


--
-- Name: tokens; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE tokens (
    id integer NOT NULL,
    user_id bigint,
    token character varying,
    token_type character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: tokens_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE tokens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tokens_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE tokens_id_seq OWNED BY tokens.id;


--
-- Name: treatment_templates; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE treatment_templates (
    id integer NOT NULL,
    author_id integer,
    treatment_type character varying,
    name character varying,
    data text,
    frequency character varying,
    period integer,
    description text,
    full_name character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    power integer DEFAULT 0,
    open_ended boolean DEFAULT false,
    editor_id integer,
    impact character varying,
    insurance character varying
);


--
-- Name: treatment_templates_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE treatment_templates_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: treatment_templates_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE treatment_templates_id_seq OWNED BY treatment_templates.id;


--
-- Name: treatments; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE treatments (
    id integer NOT NULL,
    sender_id bigint,
    receiver_id bigint,
    treatment_template_id integer,
    treatment_type character varying,
    name character varying,
    data text,
    frequency character varying,
    period integer,
    date_start date,
    date_end date,
    description text,
    status character varying,
    completion_percent numeric DEFAULT 0.0,
    full_name character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    compliance_rate integer DEFAULT 0,
    doctor_comment text,
    patient_comment text,
    open_ended boolean DEFAULT false,
    scores integer DEFAULT 0,
    dpd_id bigint,
    dpd_dosage character varying,
    version integer DEFAULT 1,
    synchronized boolean DEFAULT false
);


--
-- Name: treatments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE treatments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: treatments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE treatments_id_seq OWNED BY treatments.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE users (
    id bigint NOT NULL,
    email character varying,
    password_digest character varying,
    role character varying,
    phone character varying,
    first_name character varying,
    last_name character varying,
    full_name character varying,
    birthday date,
    gender character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    gravatar_url character varying,
    compliance_rate integer DEFAULT 0,
    total_score integer DEFAULT 0,
    current_score integer DEFAULT 0,
    "PHN" bigint,
    active boolean DEFAULT true,
    phn_validated boolean DEFAULT false,
    version integer DEFAULT 1,
    synchronized boolean DEFAULT false
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY calls ALTER COLUMN id SET DEFAULT nextval('calls_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY feed_events ALTER COLUMN id SET DEFAULT nextval('feed_events_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY measures ALTER COLUMN id SET DEFAULT nextval('measures_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY messages ALTER COLUMN id SET DEFAULT nextval('messages_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY sync_exports ALTER COLUMN id SET DEFAULT nextval('sync_exports_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY talking_stick_participants ALTER COLUMN id SET DEFAULT nextval('talking_stick_participants_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY talking_stick_rooms ALTER COLUMN id SET DEFAULT nextval('talking_stick_rooms_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY talking_stick_signals ALTER COLUMN id SET DEFAULT nextval('talking_stick_signals_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY tasks ALTER COLUMN id SET DEFAULT nextval('tasks_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY tokens ALTER COLUMN id SET DEFAULT nextval('tokens_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY treatment_templates ALTER COLUMN id SET DEFAULT nextval('treatment_templates_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY treatments ALTER COLUMN id SET DEFAULT nextval('treatments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: calls_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY calls
    ADD CONSTRAINT calls_pkey PRIMARY KEY (id);


--
-- Name: feed_events_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY feed_events
    ADD CONSTRAINT feed_events_pkey PRIMARY KEY (id);


--
-- Name: measures_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY measures
    ADD CONSTRAINT measures_pkey PRIMARY KEY (id);


--
-- Name: messages_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY messages
    ADD CONSTRAINT messages_pkey PRIMARY KEY (id);


--
-- Name: sync_exports_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY sync_exports
    ADD CONSTRAINT sync_exports_pkey PRIMARY KEY (id);


--
-- Name: talking_stick_participants_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY talking_stick_participants
    ADD CONSTRAINT talking_stick_participants_pkey PRIMARY KEY (id);


--
-- Name: talking_stick_rooms_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY talking_stick_rooms
    ADD CONSTRAINT talking_stick_rooms_pkey PRIMARY KEY (id);


--
-- Name: talking_stick_signals_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY talking_stick_signals
    ADD CONSTRAINT talking_stick_signals_pkey PRIMARY KEY (id);


--
-- Name: tasks_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY tasks
    ADD CONSTRAINT tasks_pkey PRIMARY KEY (id);


--
-- Name: tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY tokens
    ADD CONSTRAINT tokens_pkey PRIMARY KEY (id);


--
-- Name: treatment_templates_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY treatment_templates
    ADD CONSTRAINT treatment_templates_pkey PRIMARY KEY (id);


--
-- Name: treatments_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY treatments
    ADD CONSTRAINT treatments_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: index_feed_events_on_receiver_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_feed_events_on_receiver_id ON feed_events USING btree (receiver_id);


--
-- Name: index_feed_events_on_sender_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_feed_events_on_sender_id ON feed_events USING btree (sender_id);


--
-- Name: index_measures_on_patient_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_measures_on_patient_id ON measures USING btree (patient_id);


--
-- Name: index_messages_on_is_last; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_messages_on_is_last ON messages USING btree (is_last);


--
-- Name: index_messages_on_is_read; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_messages_on_is_read ON messages USING btree (is_read);


--
-- Name: index_messages_on_receiver_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_messages_on_receiver_id ON messages USING btree (receiver_id);


--
-- Name: index_messages_on_sender_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_messages_on_sender_id ON messages USING btree (sender_id);


--
-- Name: index_talking_stick_participants_on_guid; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_talking_stick_participants_on_guid ON talking_stick_participants USING btree (guid);


--
-- Name: index_talking_stick_participants_on_room_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_talking_stick_participants_on_room_id ON talking_stick_participants USING btree (room_id);


--
-- Name: index_talking_stick_signals_on_recipient_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_talking_stick_signals_on_recipient_id ON talking_stick_signals USING btree (recipient_id);


--
-- Name: index_talking_stick_signals_on_sender_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_talking_stick_signals_on_sender_id ON talking_stick_signals USING btree (sender_id);


--
-- Name: index_tasks_on_is_completed; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_tasks_on_is_completed ON tasks USING btree (is_completed);


--
-- Name: index_tasks_on_treatment_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_tasks_on_treatment_id ON tasks USING btree (treatment_id);


--
-- Name: index_tokens_on_token; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_tokens_on_token ON tokens USING btree (token);


--
-- Name: index_treatment_templates_on_editor_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_treatment_templates_on_editor_id ON treatment_templates USING btree (editor_id);


--
-- Name: index_treatments_on_receiver_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_treatments_on_receiver_id ON treatments USING btree (receiver_id);


--
-- Name: index_treatments_on_sender_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_treatments_on_sender_id ON treatments USING btree (sender_id);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_users_on_email ON users USING btree (email);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- Name: talking_stick_rooms_before_delete_row_tr; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER talking_stick_rooms_before_delete_row_tr BEFORE DELETE ON talking_stick_rooms FOR EACH ROW EXECUTE PROCEDURE talking_stick_rooms_before_delete_row_tr();


--
-- Name: tasks_after_update_of_is_completed_row_tr; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER tasks_after_update_of_is_completed_row_tr AFTER UPDATE OF is_completed ON tasks FOR EACH ROW EXECUTE PROCEDURE tasks_after_update_of_is_completed_row_tr();


--
-- Name: treatments_after_update_row_tr; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER treatments_after_update_row_tr AFTER UPDATE ON treatments FOR EACH ROW EXECUTE PROCEDURE treatments_after_update_row_tr();


--
-- Name: fk_rails_b631932c0c; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY treatment_templates
    ADD CONSTRAINT fk_rails_b631932c0c FOREIGN KEY (editor_id) REFERENCES users(id);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user",public;

INSERT INTO schema_migrations (version) VALUES ('20140620002807');

INSERT INTO schema_migrations (version) VALUES ('20140626174552');

INSERT INTO schema_migrations (version) VALUES ('20140703081750');

INSERT INTO schema_migrations (version) VALUES ('20140705054205');

INSERT INTO schema_migrations (version) VALUES ('20140714123833');

INSERT INTO schema_migrations (version) VALUES ('20140811090045');

INSERT INTO schema_migrations (version) VALUES ('20140823174431');

INSERT INTO schema_migrations (version) VALUES ('20141203042333');

INSERT INTO schema_migrations (version) VALUES ('20150112020703');

INSERT INTO schema_migrations (version) VALUES ('20150112172857');

INSERT INTO schema_migrations (version) VALUES ('20151130012253');

INSERT INTO schema_migrations (version) VALUES ('20151201083242');

INSERT INTO schema_migrations (version) VALUES ('20151202152348');

INSERT INTO schema_migrations (version) VALUES ('20151202163017');

INSERT INTO schema_migrations (version) VALUES ('20151202183520');

INSERT INTO schema_migrations (version) VALUES ('20151204151021');

INSERT INTO schema_migrations (version) VALUES ('20151217155825');

INSERT INTO schema_migrations (version) VALUES ('20151217160045');

INSERT INTO schema_migrations (version) VALUES ('20151217160227');

INSERT INTO schema_migrations (version) VALUES ('20151221170931');

INSERT INTO schema_migrations (version) VALUES ('20151221171036');

INSERT INTO schema_migrations (version) VALUES ('20151221171044');

INSERT INTO schema_migrations (version) VALUES ('20151222115320');

INSERT INTO schema_migrations (version) VALUES ('20151223144603');

INSERT INTO schema_migrations (version) VALUES ('20151223153142');

INSERT INTO schema_migrations (version) VALUES ('20151224103804');

INSERT INTO schema_migrations (version) VALUES ('20151224151726');

INSERT INTO schema_migrations (version) VALUES ('20151230092403');

INSERT INTO schema_migrations (version) VALUES ('20160126132120');

INSERT INTO schema_migrations (version) VALUES ('20160201131527');

INSERT INTO schema_migrations (version) VALUES ('20160201132339');

INSERT INTO schema_migrations (version) VALUES ('20160302083446');

INSERT INTO schema_migrations (version) VALUES ('20160303151552');

INSERT INTO schema_migrations (version) VALUES ('20160311101458');

INSERT INTO schema_migrations (version) VALUES ('20160318073422');

INSERT INTO schema_migrations (version) VALUES ('20160401131854');

INSERT INTO schema_migrations (version) VALUES ('20160414143024');

INSERT INTO schema_migrations (version) VALUES ('20160414143025');

INSERT INTO schema_migrations (version) VALUES ('20160414143026');

INSERT INTO schema_migrations (version) VALUES ('20160414143027');

INSERT INTO schema_migrations (version) VALUES ('20160427122829');

INSERT INTO schema_migrations (version) VALUES ('20160509091647');

INSERT INTO schema_migrations (version) VALUES ('20160607101506');

INSERT INTO schema_migrations (version) VALUES ('20160620091600');

INSERT INTO schema_migrations (version) VALUES ('20160620092032');

INSERT INTO schema_migrations (version) VALUES ('20160620092239');

INSERT INTO schema_migrations (version) VALUES ('20160620092541');

INSERT INTO schema_migrations (version) VALUES ('20160620092638');

INSERT INTO schema_migrations (version) VALUES ('20160620092810');

INSERT INTO schema_migrations (version) VALUES ('20160629123023');

INSERT INTO schema_migrations (version) VALUES ('20160705133650');

INSERT INTO schema_migrations (version) VALUES ('20160706081831');

INSERT INTO schema_migrations (version) VALUES ('20160706083848');

