class AddPhnValidatedToUser < ActiveRecord::Migration
  def change
    add_column :users, :phn_validated, :boolean, default: false
  end
end
