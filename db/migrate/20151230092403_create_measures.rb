class CreateMeasures < ActiveRecord::Migration
  def change
    create_table :measures do |t|
      t.decimal :value, null: false
      t.string :when
      t.date :date, null: false
      t.integer :patient_id, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
