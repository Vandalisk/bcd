class AddScoresToTreatment < ActiveRecord::Migration
  def change
    add_column :treatments, :scores, :integer, default: 0
  end
end
