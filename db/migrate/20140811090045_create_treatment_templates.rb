# encoding: utf-8

class CreateTreatmentTemplates < ActiveRecord::Migration
  def up
    create_table :treatment_templates do |t|
      t.integer :author_id,      null: true, default: nil
      t.string  :treatment_type, null: true, default: nil
      t.string  :name,           null: true, default: nil
      t.text    :data,           null: true, default: nil
      t.string  :frequency,      null: true, default: nil
      t.integer :period,         null: true, default: nil
      t.text    :description,    null: true, default: nil
      t.string  :full_name,      null: true, default: nil
      t.timestamps
    end
  end

  def down
    drop_table :treatment_templates
  end
end
