class AddOpenEndedColumnToTreatmentTemplates < ActiveRecord::Migration
  def change
    add_column :treatment_templates, :open_ended, :boolean, default: false
  end
end
