class AddInsuranceToTreatmentTemplate < ActiveRecord::Migration
  def change
    add_column :treatment_templates, :insurance, :string
  end
end
