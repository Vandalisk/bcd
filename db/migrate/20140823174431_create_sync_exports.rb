# encoding: utf-8

class CreateSyncExports < ActiveRecord::Migration
  def up
    create_table :sync_exports do |t|
      t.text    :options,     null: true, default: nil
      t.integer :total_count, null: true, default: 0
      t.integer :done_count,  null: true, default: 0
      t.integer :last_id,     null: true, default: 0
      t.string  :status,      null: true, default: nil
      t.integer :user_id,     null: true, default: nil
      t.timestamps
    end
  end

  def down
    drop_table :sync_exports
  end
end
