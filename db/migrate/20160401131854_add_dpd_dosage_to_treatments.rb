class AddDpdDosageToTreatments < ActiveRecord::Migration
  def change
    add_column :treatments, :dpd_dosage, :string
  end
end
