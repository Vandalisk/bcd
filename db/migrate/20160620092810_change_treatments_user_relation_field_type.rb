class ChangeTreatmentsUserRelationFieldType < ActiveRecord::Migration
  def up
    change_column :treatments, :sender_id, :bigint
    change_column :treatments, :receiver_id, :bigint
  end

  def down
    change_column :treatments, :sender_id, :integer
    change_column :treatments, :receiver_id, :integer
  end
end
