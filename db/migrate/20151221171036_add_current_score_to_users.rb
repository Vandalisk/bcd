class AddCurrentScoreToUsers < ActiveRecord::Migration
  def change
    add_column :users, :current_score, :integer, default: 0
    rename_column :users, :scores, :total_score
  end
end
