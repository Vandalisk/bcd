class AddComplianceRateToTreatment < ActiveRecord::Migration
  def change
    add_column :treatments, :compliance_rate, :integer, default: 0
  end
end
