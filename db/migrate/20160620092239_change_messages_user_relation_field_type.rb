class ChangeMessagesUserRelationFieldType < ActiveRecord::Migration
  def up
    change_column :messages, :sender_id, :bigint
    change_column :messages, :receiver_id, :bigint
  end

  def down
    change_column :messages, :sender_id, :integer
    change_column :messages, :receiver_id, :integer
  end
end
