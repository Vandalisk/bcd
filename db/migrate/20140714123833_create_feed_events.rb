# encoding: utf-8

class CreateFeedEvents < ActiveRecord::Migration
  def up
    create_table :feed_events do |t|
      t.string :resource_type, null: true, default: nil
      t.integer :resource_id,  null: true, default: nil
      t.integer :sender_id,    null: true, default: nil
      t.integer :receiver_id,  null: true, default: nil
      t.text :data,            null: true, default: nil
      t.timestamps
    end

    add_index :feed_events, :sender_id
    add_index :feed_events, :receiver_id
  end

  def down
    drop_table :feed_events
  end
end
