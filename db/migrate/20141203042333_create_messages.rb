# encoding: utf-8

class CreateMessages < ActiveRecord::Migration
  def up
    create_table :messages do |t|
      t.integer :sender_id,   null: true,  default: 0
      t.integer :receiver_id, null: true,  default: 0
      t.text    :text,        null: true,  default: nil
      t.boolean :is_last,     null: false, default: false
      t.boolean :is_read,     null: false, default: false
      t.timestamps
    end
  end

  def down
    drop_table :messages
  end
end
