# encoding: utf-8

class CreateTokens < ActiveRecord::Migration
  def up
    create_table :tokens do |t|
      t.integer :user_id,    null: true, default: nil
      t.string  :token,      null: true, default: nil
      t.string  :token_type, null: true, default: nil
      t.timestamps
    end

    add_index :tokens, :token, unique: true
  end

  def down
    drop_table :tokens
  end
end
