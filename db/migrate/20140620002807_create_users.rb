# encoding: utf-8

class CreateUsers < ActiveRecord::Migration
  def up
    create_table :users do |t|
      t.string :email,           null: true, default: nil
      t.string :password_digest, null: true, default: nil
      t.string :role,            null: true, default: nil
      t.string :phone,           null: true, default: nil
      t.string :first_name,      null: true, default: nil
      t.string :last_name,       null: true, default: nil
      t.string :full_name,       null: true, default: nil
      t.date   :birthday,        null: true, default: nil
      t.string :gender,          null: true, default: nil
      t.timestamps
    end

    add_index :users, :email, unique: true
  end

  def down
    drop_table :users
  end
end
