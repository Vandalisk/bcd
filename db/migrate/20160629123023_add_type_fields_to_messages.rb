class AddTypeFieldsToMessages < ActiveRecord::Migration
  def change
    add_column :messages, :message_type, :string, default: 'common'
  end
end
