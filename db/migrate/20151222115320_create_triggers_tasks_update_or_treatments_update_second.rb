# This migration was auto-generated via `rake db:generate_trigger_migration'.
# While you can edit this file, any changes you make to the definitions here
# will be undone by the next auto-generated trigger migration.

class CreateTriggersTasksUpdateOrTreatmentsUpdateSecond < ActiveRecord::Migration
  def up
    drop_trigger("tasks_after_update_of_is_completed_row_tr", "tasks", :generated => true)

    drop_trigger("treatments_after_update_row_tr", "treatments", :generated => true)

    create_trigger("tasks_after_update_of_is_completed_row_tr", :generated => true, :compatibility => 1).
        on("tasks").
        after(:update).
        of(:is_completed) do
      <<-SQL_ACTIONS
      IF NEW.is_completed THEN
        UPDATE users SET current_score = current_score + NEW.points FROM treatments
        WHERE treatments.receiver_id = users.id AND treatments.id = NEW.treatment_id;
      ELSE
        UPDATE users SET current_score = current_score - NEW.points FROM treatments
        WHERE treatments.receiver_id = users.id AND treatments.id = NEW.treatment_id;
      END IF;
      SQL_ACTIONS
    end

    create_trigger("treatments_after_update_row_tr", :generated => true, :compatibility => 1).
        on("treatments").
        after(:update) do
      <<-SQL_ACTIONS
      IF (OLD.status = 'active' AND NEW.status <> 'active') OR
        (NEW.status = 'active' AND OLD.status <>'active') OR
        OLD.frequency <> NEW.frequency OR OLD.period <> NEW.period THEN
        UPDATE users
        SET total_score = (
          SELECT SUM(points) FROM tasks
            INNER JOIN treatments
            ON tasks.treatment_id = treatments.id
            WHERE treatments.receiver_id = NEW.receiver_id
        )
        WHERE users.id = NEW.receiver_id;
      END IF;
      SQL_ACTIONS
    end
  end

  def down
    drop_trigger("tasks_after_update_of_is_completed_row_tr", "tasks", :generated => true)

    drop_trigger("treatments_after_update_row_tr", "treatments", :generated => true)

    create_trigger("tasks_after_update_of_is_completed_row_tr", :generated => true, :compatibility => 1).
        on("tasks").
        after(:update).
        of(:is_completed) do
      <<-SQL_ACTIONS
      UPDATE treatments SET scores = (
        SELECT SUM(points) FROM tasks
          WHERE tasks.is_completed = true AND tasks.treatment_id = NEW.treatment_id
      ) WHERE treatments.id = NEW.treatment_id;

      UPDATE users SET scores = (
        SELECT SUM(points) FROM tasks
          INNER JOIN treatments
          ON tasks.treatment_id = treatments.id
          WHERE treatments.receiver_id = (
              SELECT receiver_id
              FROM treatments
              WHERE treatments.id = NEW.treatment_id
            )
            AND tasks.is_completed = true
      ) WHERE users.id = (
              SELECT receiver_id
              FROM treatments
              WHERE treatments.id = NEW.treatment_id
            );
      SQL_ACTIONS
    end

    create_trigger("treatments_after_update_row_tr", :generated => true, :compatibility => 1).
        on("treatments").
        after(:update) do
      <<-SQL_ACTIONS
      IF (OLD.status = 'active' AND NEW.status <> 'active') OR (NEW.status = 'active' AND OLD.status <>'active') OR OLD.frequency <> NEW.frequency OR OLD.period <> NEW.period THEN
        UPDATE users
        SET total_score = (
          SELECT SUM(tasks.points) FROM tasks
            INNER JOIN treatments
            ON tasks.treatment_id = treatments.id
            WHERE treatments.receiver_id = NEW.receiver_id
        )
        WHERE users.id = NEW.receiver_id;
      END IF;
      SQL_ACTIONS
    end
  end
end
