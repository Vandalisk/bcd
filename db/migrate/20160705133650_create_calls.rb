class CreateCalls < ActiveRecord::Migration
  def change
    create_table :calls do |t|
      t.integer :room_id
      t.datetime :started_at
      t.datetime :ended_at
      t.integer :caller_id
      t.integer :recipient_id
      t.string :duration

      t.timestamps null: false
    end
  end
end
