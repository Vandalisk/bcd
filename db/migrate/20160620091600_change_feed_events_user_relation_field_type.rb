class ChangeFeedEventsUserRelationFieldType < ActiveRecord::Migration
  def up
    change_column :feed_events, :sender_id, :bigint
    change_column :feed_events, :receiver_id, :bigint
  end

  def down
    change_column :feed_events, :sender_id, :integer
    change_column :feed_events, :receiver_id, :integer
  end
end
