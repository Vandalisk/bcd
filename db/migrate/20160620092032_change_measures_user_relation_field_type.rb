class ChangeMeasuresUserRelationFieldType < ActiveRecord::Migration
  def up
    change_column :measures, :patient_id, :bigint
  end

  def down
    change_column :measures, :patient_id, :integer
  end
end
