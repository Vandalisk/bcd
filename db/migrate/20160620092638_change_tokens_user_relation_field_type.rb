class ChangeTokensUserRelationFieldType < ActiveRecord::Migration
  def up
    change_column :tokens, :user_id, :bigint
  end

  def down
    change_column :tokens, :user_id, :integer
  end
end
