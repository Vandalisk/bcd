class AddScoresToUser < ActiveRecord::Migration
  def change
    add_column :users, :scores, :integer, default: 0
  end
end
