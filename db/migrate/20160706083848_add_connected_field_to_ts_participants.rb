class AddConnectedFieldToTsParticipants < ActiveRecord::Migration
  def change
    add_column :talking_stick_participants, :connected, :boolean
  end
end
