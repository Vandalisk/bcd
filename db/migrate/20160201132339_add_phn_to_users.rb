class AddPhnToUsers < ActiveRecord::Migration
  def change
    add_column :users, :PHN, :integer, limit: 8
    change_column :users, :id, :integer, limit: 8
  end
end
