class AddFieldsToDpdSynchronizationTables < ActiveRecord::Migration
  def change
    add_column :treatments, :version, :integer, default: 1
    add_column :users, :version, :integer, default: 1
    add_column :treatments, :synchronized, :boolean, default: false
    add_column :users, :synchronized, :boolean, default: false
  end
end
