# encoding: utf-8

class AddIndexesToMessages < ActiveRecord::Migration
  def up
    add_index :messages, :sender_id
    add_index :messages, :receiver_id
    add_index :messages, :is_last
    add_index :messages, :is_read
  end

  def down
    remove_index :messages, :sender_id
    remove_index :messages, :receiver_id
    remove_index :messages, :is_last
    remove_index :messages, :is_read
  end
end
