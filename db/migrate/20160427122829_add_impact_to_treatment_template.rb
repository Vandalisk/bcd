class AddImpactToTreatmentTemplate < ActiveRecord::Migration
  def change
    add_column :treatment_templates, :impact, :string
  end
end
