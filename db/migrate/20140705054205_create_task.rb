# encoding: utf-8

class CreateTask < ActiveRecord::Migration
  def up
    create_table :tasks do |t|
      t.integer :treatment_id,   null: true,  default: nil
      t.date    :date,           null: true,  default: nil
      t.boolean :is_completed,   null: false, default: false
      t.text    :data_completed, null: true,  default: nil
      t.text    :comment,        null: true,  default: nil
      t.timestamps
    end

    add_index :tasks, :treatment_id
    add_index :tasks, :is_completed
  end

  def down
    drop_table :tasks
  end
end
