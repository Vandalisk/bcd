# encoding: utf-8

class AddGravatarUrlToUsers < ActiveRecord::Migration
  def up
    add_column :users, :gravatar_url, :string, null: true, default: nil
  end

  def down
    remove_column :users, :gravatar_url
  end
end
