class ChangeSyncExportsUserRelationFieldType < ActiveRecord::Migration
  def up
    change_column :sync_exports, :user_id, :bigint
  end

  def down
    change_column :sync_exports, :user_id, :integer
  end
end
