class AddComplianceToUsers < ActiveRecord::Migration
  def change
    add_column :users, :compliance_rate, :integer, default: 0
  end
end
