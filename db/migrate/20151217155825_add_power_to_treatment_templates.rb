class AddPowerToTreatmentTemplates < ActiveRecord::Migration
  def change
    add_column :treatment_templates, :power, :integer, default: 0
  end
end
