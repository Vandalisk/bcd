class AddPatientCommentToTreatment < ActiveRecord::Migration
  def change
    add_column :treatments, :patient_comment, :text
  end
end
