# This migration was auto-generated via `rake db:generate_trigger_migration'.
# While you can edit this file, any changes you make to the definitions here
# will be undone by the next auto-generated trigger migration.

class CreateTriggersTalkingStickRoomsDeleteOrTreatmentsUpdate < ActiveRecord::Migration
  def up
    drop_trigger("treatments_after_update_row_tr", "treatments", :generated => true)

    create_trigger("talking_stick_rooms_before_delete_row_tr", :generated => true, :compatibility => 1).
        on("talking_stick_rooms").
        before(:delete) do
      <<-SQL_ACTIONS
        UPDATE calls SET ended_at = now(),
                         duration = age(OLD.last_used::timestamp, started_at::timestamp)
                         WHERE calls.room_id = OLD.id;
      SQL_ACTIONS
    end

    create_trigger("treatments_after_update_row_tr", :generated => true, :compatibility => 1).
        on("treatments").
        after(:update) do
      <<-SQL_ACTIONS
      IF OLD.status = 'active' AND NEW.status <> 'active' THEN
        UPDATE users SET total_score = total_score - NEW.scores,
                         current_score = current_score - round(NEW.scores *
                            cast((SELECT COUNT(*) FROM tasks WHERE tasks.treatment_id = NEW.id AND tasks.is_completed = true) as float)
                            /
                            (SELECT COUNT(*) FROM tasks WHERE tasks.treatment_id = NEW.id)
                         )
                         WHERE users.id = NEW.receiver_id;
      ELSIF NEW.status = 'active' AND OLD.status <>'active' THEN
        UPDATE users SET total_score = total_score + NEW.scores WHERE users.id = NEW.receiver_id;
      END IF;
      SQL_ACTIONS
    end
  end

  def down
    drop_trigger("talking_stick_rooms_before_delete_row_tr", "talking_stick_rooms", :generated => true)

    drop_trigger("treatments_after_update_row_tr", "treatments", :generated => true)

    create_trigger("treatments_after_update_row_tr", :generated => true, :compatibility => 1).
        on("treatments").
        after(:update) do
      <<-SQL_ACTIONS
      IF OLD.status = 'active' AND NEW.status <> 'active' THEN
        UPDATE users SET total_score = total_score - NEW.scores,
                         current_score = current_score - round(NEW.scores *
                            cast((SELECT COUNT(*) FROM tasks WHERE tasks.treatment_id = NEW.id AND tasks.is_completed = true) as float)
                            /
                            (SELECT COUNT(*) FROM tasks WHERE tasks.treatment_id = NEW.id)
                         )
                         WHERE users.id = NEW.receiver_id;
      ELSIF NEW.status = 'active' AND OLD.status <> 'active' THEN
        UPDATE users SET total_score = total_score + NEW.scores WHERE users.id = NEW.receiver_id;
      END IF;
      SQL_ACTIONS
    end
  end
end
