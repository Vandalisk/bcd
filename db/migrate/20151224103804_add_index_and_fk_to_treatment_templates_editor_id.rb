class AddIndexAndFkToTreatmentTemplatesEditorId < ActiveRecord::Migration
  def change
    add_index :treatment_templates, :editor_id
    add_foreign_key :treatment_templates, :users, column: :editor_id, dependent: :nullify
  end
end
