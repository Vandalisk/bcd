class AddOpenEndedFieldToTreatments < ActiveRecord::Migration
  def change
    add_column :treatments, :open_ended, :boolean, default: false
  end
end
