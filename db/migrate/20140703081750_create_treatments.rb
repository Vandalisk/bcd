# encoding: utf-8

class CreateTreatments < ActiveRecord::Migration
  def up
    create_table :treatments do |t|
      t.integer :sender_id,             null: true, default: nil
      t.integer :receiver_id,           null: true, default: nil
      t.integer :treatment_template_id, null: true, default: nil
      t.string  :treatment_type,        null: true, default: nil
      t.string  :name,                  null: true, default: nil
      t.text    :data,                  null: true, default: nil
      t.string  :frequency,             null: true, default: nil
      t.integer :period,                null: true, default: nil
      t.date    :date_start,            null: true, default: nil
      t.date    :date_end,              null: true, default: nil
      t.text    :description,           null: true, default: nil
      t.string  :status,                null: true, default: nil
      t.decimal :completion_percent,    null: true, default: 0.00
      t.string  :full_name,             null: true, default: nil
      t.timestamps
    end

    add_index :treatments, :sender_id
    add_index :treatments, :receiver_id
  end

  def down
    drop_table :treatments
  end
end
