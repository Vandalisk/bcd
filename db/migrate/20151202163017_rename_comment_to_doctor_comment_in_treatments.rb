class RenameCommentToDoctorCommentInTreatments < ActiveRecord::Migration
  def change
    rename_column :treatments, :comment, :doctor_comment
  end
end
