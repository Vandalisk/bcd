class AddAuthorIdToTreatmentTemplates < ActiveRecord::Migration
  def change
    add_column :treatment_templates, :editor_id, :integer
  end
end
