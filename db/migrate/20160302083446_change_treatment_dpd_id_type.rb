class ChangeTreatmentDpdIdType < ActiveRecord::Migration
  def up
    change_column :treatments, :dpd_id, :bigint
  end

  def down
    change_column :treatments, :dpd_id, :integer
  end
end
