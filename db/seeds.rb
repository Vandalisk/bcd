require 'ffaker'
require 'active_support/testing/time_helpers'
include ActiveSupport::Testing::TimeHelpers

class FakePopulator
  def self.populate!
    p User.create({
      email:      "1@1.com",
      password:   "1111",
      role:       "admin",
      password:   "1111",
      first_name: "Oleksandr",
      last_name:  "Sergiienko",
      phone:      "123-456-789",
      birthday:   "1986-08-26",
      gender:     "male"
    })
    return if ENV['RAILS_ENV'] == 'production'

    60.times do
      u = User.create({
        email:      FFaker::Internet.email,
        password:   "1111",
        role:       ["doctor", "patient", "patient", "patient", "patient", "patient"][rand(6)],
        password:   "1111",
        first_name: FFaker::Name.first_name,
        last_name:  FFaker::Name.last_name,
        phone:      FFaker::PhoneNumber.short_phone_number,
        birthday:   Time.at(rand(60.years.ago.to_i..20.years.ago.to_i)).to_date,
        gender:     ["male","female"][rand(2)],
      })
      p u
    end

    tps = [
      { name: 'Run', data: { count: "50", condition: "30 minutes 1 time/day" }, treatment_type: 'exercise', frequency: 'daily', period: 3, author_id: 1 },
      { name: 'Quit smoking', data: { count: "50", condition: "Less then 5 sigarettes/day" }, treatment_type: 'exercise', frequency: 'daily', period: 3, author_id: 1 },
      { name: 'Swimming', data: { count: "50", condition: "30 minutes" }, treatment_type: 'exercise', frequency: 'daily', period: 3, author_id: 1 },
      { name: 'Desserts', treatment_type: 'lifestyle', frequency: 'daily', period: 3, author_id: 1, open_ended: true },
      { name: 'Low carbohydrate / starch', treatment_type: 'lifestyle', frequency: 'daily', period: 3, author_id: 1, open_ended: true },
      { name: 'No snacking', treatment_type: 'lifestyle', frequency: 'daily', period: 3, author_id: 1, open_ended: true },
      { name: 'No desserts', treatment_type: 'lifestyle', frequency: 'daily', period: 5, author_id: 1, open_ended: true },
      { name: 'Metformin', data: { unit: "mg/ml", dosage: "500", when: "with", schedule: { breakfast: "2" , dinner: "2" } }, treatment_type: 'medication_tablets', frequency: 'daily', period: 3, author_id: 1 },
      { name: 'Metformin SR', data: { unit: "mg tabs", dosage: "500", when: "with", schedule: { breakfast: "4" } }, treatment_type: 'medication_tablets', frequency: 'daily', period: 3, author_id: 1 },
      { name: 'Metformin SR', data: { unit: "mg tabs", dosage: "1000", when: "with", schedule: { breakfast: "2" } }, treatment_type: 'medication_tablets', frequency: 'daily', period: 3, author_id: 1 },
      { name: 'Glyburide', data: { unit: "mg tabs", dosage: "5", when: "with", schedule: { breakfast: "2", dinner: "2" } }, treatment_type: 'medication_tablets', frequency: 'daily', period: 3, author_id: 1 },
      { name: 'Gliclazide SR', data: { unit: "mg tabs", dosage: "30", when: "with", schedule: { breakfast: "2" } }, treatment_type: 'medication_tablets', frequency: 'daily', period: 3, author_id: 1 },
      { name: 'Linagliptin', data: { unit: "mg tabs", dosage: "5", when: "with", schedule: { breakfast: "1" } }, treatment_type: 'medication_tablets', frequency: 'daily', period: 3, author_id: 1 },
      { name: 'Saxagliptin', data: { unit: "mg tabs", dosage: "5", when: "with", schedule: { breakfast: "1" } }, treatment_type: 'medication_tablets', frequency: 'daily', period: 3, author_id: 1 },
      { name: 'Canagliflozin', data: { unit: "mg tabs", dosage: "300", when: "with", schedule: { breakfast: "1" } }, treatment_type: 'medication_tablets', frequency: 'daily', period: 3, author_id: 1 },
      { name: 'Dapagliflozin', data: { unit: "mg tabs", dosage: "10", when: "with", schedule: { breakfast: "1" } }, treatment_type: 'medication_tablets', frequency: 'daily', period: 3, author_id: 1 },
      { name: 'Empagliflozin', data: { unit: "mg tabs", dosage: "25", when: "with", schedule: { breakfast: "1" } }, treatment_type: 'medication_tablets', frequency: 'daily', period: 3, author_id: 1 },
      { name: 'Ertugliflozin', data: { unit: "mg tabs", dosage: "15", when: "with", schedule: { breakfast: "1" } }, treatment_type: 'medication_tablets', frequency: 'daily', period: 3, author_id: 1 },
      { name: 'Atorvastatin', data: { unit: "mg tabs", dosage: "40", when: "with", schedule: { breakfast: "1" } }, treatment_type: 'medication_tablets', frequency: 'daily', period: 3, author_id: 1 },
      { name: 'Rosuvastatin', data: { unit: "mg tabs", dosage: "40", when: "with", schedule: { breakfast: "1" } }, treatment_type: 'medication_tablets', frequency: 'daily', period: 3, author_id: 1 },
      { name: 'Ramipril', data: { unit: "mg caps", dosage: "10", when: "with", schedule: { breakfast: "1" } }, treatment_type: 'medication_injection', frequency: 'daily', period: 3, author_id: 1 },
      { name: 'HCTZ', data: { unit: "mg caps", dosage: "12.5", when: "with", schedule: { breakfast: "1" } }, treatment_type: 'medication_injection', frequency: 'daily', period: 3, author_id: 1 },
      { name: 'Spironolactone', data: { unit: "mg caps", dosage: "25", when: "with", schedule: { breakfast: "1" } }, treatment_type: 'medication_injection', frequency: 'daily', period: 3, author_id: 1 },
      { name: 'Bisoprolol', data: { unit: "mg caps", dosage: "5", when: "with", schedule: { breakfast: "1" } }, treatment_type: 'medication_injection', frequency: 'daily', period: 3, author_id: 1 },
      { name: 'Amlopidine', data: { unit: "mg caps", dosage: "5", when: "with", schedule: { breakfast: "1" } }, treatment_type: 'medication_injection', frequency: 'daily', period: 3, author_id: 1 },
      { name: 'Terazosin', data: { unit: "mg caps", dosage: "1", when: "at", schedule: { bedtime: "1" } }, treatment_type: 'medication_injection', frequency: 'daily', period: 3, author_id: 1 },
      { name: 'Insulin Glargine', data: { unit: "u/ml", dosage: "100", when: "before", schedule: { breakfast: "50" } }, description: "adjust dose targeting sugar before breakfast of 5.0-7.0", treatment_type: 'medication_injection', frequency: 'daily', period: 3, author_id: 1 },
      { name: 'Insulin Detemir', data: { unit: "u/ml", dosage: "100", when: "before", schedule: { breakfast: "50" } }, description: "adjust dose targeting sugar before breakfast of 5.0-7.0", treatment_type: 'medication_injection', frequency: 'daily', period: 3, author_id: 1 },
      { name: 'Insulin Glulisine', data: { unit: "u/ml", dosage: "100", when: "before", schedule: { dinner: "4" } }, description: "adjust dose targeting sugar 2 hours after meal of 6.0-10.0", treatment_type: 'medication_injection', frequency: 'daily', period: 3, author_id: 1 },
      { name: 'Insulin Lispro', data: { unit: "u/ml", dosage: "100", when: "before", schedule: { dinner: "4" } }, description: "adjust dose targeting sugar 2 hours after meal of 6.0-10.0", treatment_type: 'medication_injection', frequency: 'daily', period: 3, author_id: 1 },
      { name: 'Insulin Aspart', data: { unit: "u/ml", dosage: "100", when: "before", schedule: { dinner: "4" } }, description: "adjust dose targeting sugar 2 hours after meal of 6.0-10.0", treatment_type: 'medication_injection', frequency: 'daily', period: 3, author_id: 1 },
      { name: 'Liraglutide', data: { unit: "u/ml", dosage: "6", when: "before", schedule: { breakfast: "1.8" } }, description: "stomach side-effects permitting", treatment_type: 'medication_injection', frequency: 'daily', period: 3, author_id: 1 },
      { name: 'Tadalafil', data: { unit: "mg/ml", dosage: "5", when: "before", schedule: { breakfast: "1" } }, treatment_type: 'medication_injection', frequency: 'daily', period: 3, author_id: 1 },
      { name: 'ASA', data: { unit: "mg/ml", dosage: "81", when: "before", schedule: { breakfast: "1" } }, treatment_type: 'medication_injection', frequency: 'daily', period: 3, author_id: 1 },
      { name: 'Levothyroxine', data: { unit: "mg/ml", dosage: "0.1", when: "with", schedule: { breakfast: "1" } }, treatment_type: 'medication_injection', frequency: 'daily', period: 3, author_id: 1 },
      { name: 'Methimazole', data: { unit: "mg/ml", dosage: "5", when: "with", schedule: { breakfast: "1" } }, treatment_type: 'medication_injection', frequency: 'daily', period: 3, author_id: 1 },
      { name: 'Medroxyprogesterone', data: { unit: "mg/ml", dosage: "10", when: "with", schedule: { breakfast: "1" } }, treatment_type: 'medication_injection', frequency: 'daily', period: 3, author_id: 1 },
      { name: 'Progesterone Micronized', data: { unit: "mg/ml", dosage: "100", when: "at", schedule: { bedtime: "2" } }, treatment_type: 'medication_injection', frequency: 'daily', period: 3, author_id: 1 },
    ]
    tps.each { |tp| p TreatmentTemplate.create!(tp.merge({ description: FFaker::Lorem.paragraph, power: rand(1..10), insurance: ['yes', 'no', 'partial', 'yes_with_authority'].sample, impact: ['high_a1c', 'cholesterol/apob', 'high_blood_pressure', 'other'].sample })) }

    treatment_templates = TreatmentTemplate.all.to_a
    treatment_templates_size = treatment_templates.size

    # TreatmentTemplate.where(treatment_type: "medication").all.each{ |rec| oc = rec.data; os = oc["schedule"]; data = os.to_a.select{ |a| a[1]["amount"].to_f > 0 }; rec.data = { "unit" => oc["unit"], "dosage" => oc["dosage"], "when" => data[0][1]["when"], "schedule" => data.inject({}){ |h, a| h[a[0]] = a[1]["amount"]; h } }; rec.save }
    # Task.joins("LEFT JOIN treatments ON tasks.treatment_id = treatments.id").where("treatments.treatment_type = 'medication'").where(is_completed: true).all.each{ |rec| oc = rec.data_completed; os = oc["schedule"]; data = os.to_a.select{ |a| a[1]["amount"].to_f > 0 }; rec.data_completed = { "unit" => oc["unit"], "dosage" => oc["dosage"], "when" => data[0][1]["when"], "schedule" => data.inject({}){ |h, a| h[a[0]] = a[1]["amount"]; h } }; rec.save }

    sender_ids  = User.select(:id).where("role='doctor' OR role='admin'").pluck(:id)
    sender_ids_size = sender_ids.size
    receiver_ids = User.select(:id).where(role: "patient").pluck(:id)
    receiver_ids_size = receiver_ids.size

    500.times do
      treatment_template = treatment_templates[rand(treatment_templates_size)]
      if treatment_template
        t = Treatment.create(
          sender_id: sender_ids[rand(sender_ids_size)],
          receiver_id: receiver_ids[rand(receiver_ids_size)],
          treatment_template_id: treatment_template.id,
          treatment_type: treatment_template.treatment_type,
          name: treatment_template.name,
          data: treatment_template.data,
          description: FFaker::Lorem.paragraph,
          frequency: "daily",
          period: [2, 7][rand(2)],
          status: "new",
        )
        p t
      end

      status = ['new', 'new', 'active', 'declined'][rand(4)]
      t.update(status: status) if status != t.status
    end
  end
end

travel_to(5.days.ago) do
  FakePopulator.populate!
end
