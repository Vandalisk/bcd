class FillScores < ActiveRecord::Migration
  def self.up
    Treatment.active.each do |treatment|
      treatment.set_scores!
    end

    User.patients.each do |patient|
      patient.set_scores!
    end
  end

  def self.down
    raise IrreversibleMigration
  end
end
