class MarkOutdatedTreatmentsAsCompleted < ActiveRecord::Migration
  def self.up
    Treatment.outdated.each do |t|
      t.update(status: 'completed')
    end
  end

  def self.down
    raise IrreversibleMigration
  end
end
