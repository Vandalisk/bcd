# encoding: utf-8

module Coders
  class CsvColumn
    class << self
      def dump(arr)
        v = arr.join(",") rescue nil
        v = v.size > 0 ? ",#{v}," : nil rescue nil
        v
      end

      def load(scv)
        scv.gsub(/\A,|,\Z/, "").split(",") rescue []
      end
    end
  end
end
