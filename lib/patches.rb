# encoding: utf-8

class Object
  def _debug(*args)
    Rails.logger.debug "!!!!!"
    args.each do |arg|
      Rails.logger.debug "!!!!!" + arg.inspect
    end
    Rails.logger.debug "!!!!!"
  end

  # constant-time comparison algorithm to prevent timing attacks
  def secure_compare(a, b)
    return false if a.blank? || b.blank? || a.bytesize != b.bytesize
    l = a.unpack "C#{a.bytesize}"

    res = 0
    b.each_byte { |byte| res |= byte ^ l.shift }
    res == 0
  end
end
