class BunnyFactory
  def self.build
    Rails.logger.info "Create new RabbitMQ connection"
    Bunny.new(
      host: ENV["RABBIT_HOST"],
      port: ENV["RABBIT_PORT"],
      user: ENV["RABBIT_USER"],
      pass: ENV["RABBIT_PASS"],
      tls: true,
      tls_cert: "config/cert.pem",
      tls_key: "config/key.pem",
      tls_ca_certificates: ["config/rootCA.pem"],
      verify_peer: true
    )
  end
end
