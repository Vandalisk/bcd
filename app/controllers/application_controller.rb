# encoding: utf-8

# class ApplicationController < ActionController::Base
class ApplicationController < ActionController::API
  include ActionController::HttpAuthentication::Token::ControllerMethods
  include ActionController::StrongParameters
  include ActionController::RequestForgeryProtection
  include ActionController::Rescue
  include Concerns::ErrorHandling
  protect_from_forgery
  include Pundit

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  # before_action :force_response_format
  before_action :authenticate
  before_action :require_user

  skip_before_action :require_user, only: :handle_exception

private

  # def force_response_format
  #   request.format = :json
  # end

  def authenticate
    authenticate_with_http_token do |token, options|
      @current_access_token = Token.where(token: token, token_type: 'access').first
      @current_user = @current_access_token.user if @current_access_token
    end
  end

  def user_not_authorized
    halt 403, I18n.t('error.forbidden')
  end

  def require_user #401
    halt :unauthorized, I18n.t('error.unauthorized') unless @current_user
  end

  def require_admin #403
    halt :forbidden, I18n.t('error.forbidden') if !(@current_user && @current_user.admin?)
  end

  def current_user
    @current_user
  end

  def current_access_token
    @current_access_token
  end

  def faye_client
    return @faye_client if defined? @faye_client
    @faye_client = Faye::Client.new "#{ENV['URL_API']}#{ENV['FAYE_MOUNT']}"
    @faye_client.add_extension(FayeExt::ClientToken.new(current_access_token.token))
    @faye_client
  end
end
