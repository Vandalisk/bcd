# require_dependency "talking_stick/application_controller"

module TalkingStick
  class ParticipantsController < ResourcesController
    before_action :set_room, except: :ice_connected
    before_action :set_participant, only: [:show, :edit, :update, :destroy]

    # GET /participants
    def index
      @participants = Participant.all
    end

    # GET /participants/1
    def show
    end

    # GET /participants/new
    def new
      @participant = Participant.new
    end

    # GET /participants/1/edit
    def edit
    end

    def ice_connected
      room = Room.find(params[:room_id])
      participant = room.participants.find_by_name(current_user.full_name)
      participant.update(connected: true)
      call = Call.find_by_room_id(params[:room_id])
      call.update(started_at: DateTime.now) if room.participants.connected.count == 2 && !call.started_at
    end

    # POST /participants
    def create
      new_params = participant_params.merge ip: request.remote_ip
      @participant = Participant.new(new_params)
      @participant.room = @room
      if @room.participants.count < 2 || @room.participants.find_by_name(current_user.full_name)
        old_participant = @room.participants.find_by_name(current_user.full_name)
        old_participant.delete if old_participant
        # если один дисконнектнулся - а другой в это время пинганул - то... создается новая рума? не так-то быстро,
        # существует маленькое окно между нажатием красной трубки - и удалением рума,
        if @participant.save
          WsNotifier.notify_call(faye_client, params[:userId], current_user, 'call_start', {room_id: @room.id})
          call = Call.find_or_create_by(room_id: @room.id)
          if @room.participants.count == 1 && !call.caller_id
            call.update(caller_id: current_user.id)
          elsif @room.participants.count == 2 && !call.recipient_id
            call.update(recipient_id: current_user.id)
          end
          render json: @participant
        end
      else
        WsNotifier.notify_call(faye_client, current_user.id, {id: params[:userId]}, 'busy', {room_id: @room.id})
        render json: {result: false}
      end
    end

    # PATCH/PUT /participants/1
    def update
      if @participant.update(participant_params)
        redirect_to [@room, @participant], notice: 'Participant was successfully updated.'
      else
        render :edit
      end
    end

    # DELETE /participants/1
    def destroy
      @participant.destroy
      redirect_to participants_url, notice: 'Participant was successfully destroyed.'
    end

    private
      def set_room
        @room = Room.find_or_create(slug: params[:room_id])
      end

      # Use callbacks to share common setup or constraints between actions.
      def set_participant
        @participant = Participant.find params[:id]
      end

      # Only allow a trusted parameter "white list" through.
      def participant_params
        params.require(:participant).permit(:name, :ip, :guid)
      end
  end
end
