# encoding: utf-8

class TasksController < ResourcesController

  def index
    tasks = TasksControllerPolicy::Scope.new(current_user, Task, params).resolve_list
    tasks = tasks.search(params)
    tasks = tasks.order(order_params)
    tasks = tasks.paginate(pagination_params)

    treatment_ids = tasks.map(&:treatment_id).uniq
    treatments = Treatment.where(id: treatment_ids).all

    render json: {
      tasks: tasks.map{ |v| v.prepare_for_json },
      treatments: treatments.map{ |v| v.prepare_for_json }
    }
  end

  def update
    if @item.update(permitted_params.merge({is_completed: !@item.is_completed}))
      TasksControllerPolicy::Scope.new(current_user, @item, params).resolve_update
      unless ENV['RAILS_ENV'] == 'test'
        WsNotifier.notify_task(faye_client, @item.patient.id, current_user, 'updated', @item.prepare_for_json)
      end
    else
      halt 422, I18n.t('error.validation') do |data|
        data[:errors] = @item.errors.full_messages
      end
    end
    render json: { score: @item.patient.current_score, is_completed: @item.is_completed }
  end

  def update_all
    if params[:collection].is_a?(Array)
      resource_class.transaction do
        params[:collection].each do |row|
          item = resource_class.find(row[:id])
          TasksControllerPolicy::Scope.new(current_user, item, params).resolve_update
          pp = row.permit(*resource_class::FIELDS_FOR_FORMS).tap do |whitelisted|
            whitelisted[:data_completed] = row[:data_completed] if row[:data_completed]
          end
          unless item.update pp
            halt 422, I18n.t('error.validation') do |data|
              data[:errors] = item.errors.full_messages
            end
          end
        end
      end
    end
    # TODO: improve response
    render nothing: true
  end

private

  def resource_class
    Task
  end

  def order_params
    'id asc'
  end

  def permitted_params
    params.permit(*resource_class::FIELDS_FOR_FORMS).tap do |whitelisted|
      whitelisted[:data_completed] = params[:data_completed] if params[:data_completed]
    end
  end
end
