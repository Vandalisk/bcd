# encoding: utf-8

class TreatmentTemplatesController < ResourcesController
  def index
    authorize self
    super do |treatment_templates|
      if params[:patient_id].present? || current_user.patient?
        patient_id = params[:patient_id] || current_user.id
        treatments = Treatment.where(
          receiver_id: patient_id,
          status: %w(new requested active)
        )
        treatment_ids = treatments.pluck(:treatment_template_id)
        treatment_templates.where.not(id: treatment_ids).includes(:author)
      else
        treatment_templates.includes(:author, :editor)
      end
    end
  end

  def create
    authorize self
    super do |item|
      item.author_id = current_user.id
    end
  end

  def update
    authorize self
    super do |item|
      item.editor_id = current_user.id
    end
  end

  def check_by_name
    result = TreatmentTemplate.find_by_name(params['name'])

    if params['id'] && result
      result = false if result.id == params['id'].to_i
    end

    render json: { item: !!result }
  end

private

  def resource_class
    TreatmentTemplate
  end

  def order_params
    params[:order] || 'id desc'
  end

  def permitted_params
    params.permit(*resource_class::FIELDS_FOR_FORMS).tap do |whitelisted|
      whitelisted[:data] = params[:data] if params[:data]
    end
  end
end
