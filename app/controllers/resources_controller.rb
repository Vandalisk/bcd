# encoding: utf-8

class ResourcesController < ApplicationController
  before_action :find_parent_item
  before_action :find_item, only: [:show, :update, :destroy]

  def index
    @collection = resource_scope.all

    @collection = yield @collection if block_given?

    @collection = @collection.search(params)
    @collection = @collection.order(order_params)
    @collection = @collection.paginate(pagination_params)

    render json: { collection: @collection.map{ |v| v.prepare_for_json } }
  end

  def show
    render json: { item: @item.prepare_for_json }
  end

  def create
    @item = resource_scope.new(permitted_params)
    yield @item if block_given?
    if @item.save
      render json: { item: @item.prepare_for_json }, status: 201
    else
      halt 422, I18n.t('error.validation') do |data|
        data[:errors] = @item.errors.full_messages
      end
    end
  end

  def update
    yield @item if block_given?
    if @item.update(permitted_params)
      render json: { item: @item.prepare_for_json }
    else
      halt 422, I18n.t('error.validation') do |data|
        data[:errors] = @item.errors.full_messages
      end
    end
  end

  def destroy
    @item.destroy
    render nothing: true
  end

  def uniq
    field = params[:field] if params[:field].is_a?(String)
    halt(422, 'invalid params') and return if !resource_class.column_names.include?(field)

    p = params.dup
    q = p.delete(:q)

    data = resource_scope.all
    data = data.where(["#{field} ILIKE ?", "%#{q}%"]) if q
    data = data.search(p)
    data = data.order(field).limit(params[:per_page]).uniq.pluck(field)

    render json: { collection: data }
  end

protected

  def resource_class
    raise "#resource_class needs to be implemented in child controller"
  end
  helper_method :resource_class

  def resource_scope
    resource_class
  end

  def find_parent_item
  end

  def find_item
    @item = resource_scope.find(params[:id])
  rescue
    halt 404
  end

  def permitted_params
    params.permit(*resource_class::FIELDS_FOR_FORMS)
  end

  def default_per_page
    100
  end

  def order_params
    "id asc" #TODO: implement
  end

  def pagination_params
    {
      page: !params[:page].blank? && params[:page].is_a?(String) && params[:page].to_i > 0 ? params[:page].to_i : 1,
      per_page: !params[:per_page].blank? && params[:per_page].is_a?(String) && params[:per_page].to_i > 0 ? params[:per_page].to_i : default_per_page
    }
  end
end
