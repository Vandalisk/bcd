module Concerns
  module ErrorHandling
    extend ActiveSupport::Concern

    def handle_exception
      exception = env["action_dispatch.exception"]
      status = ActionDispatch::ExceptionWrapper.new(env, exception).status_code
      # halt status, exception.message
      halt status
    end

  private

    def halt(status, message = nil)
      status = Rack::Utils.status_code(status) if status.is_a?(Symbol)
      message = Rack::Utils::HTTP_STATUS_CODES[status] if message.nil?
      data = { message: message }

      yield data if block_given?

      render json: data, status: status
    end
  end
end
