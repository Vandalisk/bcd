# encoding: utf-8

class UsersController < ResourcesController
  skip_before_action :require_user, only: :check_phn

  def create
    @user = resource_scope.new(permitted_params)
    if @user.save
      @token = @user.generate_token!('invite')
      UserMailer.invitation(user: @user, token: @token).deliver_later

      render json: { item: @user.prepare_for_json }, status: 201
    else
      halt 422, I18n.t('error.validation') do |data|
        data[:errors] = @user.errors.full_messages
      end
    end
  end

  def show
    authorize self
    super
  end

  def update
    find_item
    authorize self
    @item.assign_attributes(permitted_params)

    if @item.valid?
      if ENV['RAILS_ENV'] != 'development' && @item.patient? && User::FIELDS_FOR_UPDATE.any?{ |argument| @item.changed.include?(argument) }
        ToDPD::Notifier.new('dpd.user.update').notify do
          @item.save
          @item
        end
      else
        @item.save
      end
    else
      halt 422, I18n.t('error.validation') do |data|
        data[:errors] = @item.errors.full_messages
      end
    end
  end

  def calculate_completed_tasks
    find_item
    authorize @item
    tasks = @item.tasks
    data = {}
    %w(lifestyle medication_tablets medication_injection exercise).each do |treatment_type|
      data[treatment_type] = { need: 0, completed: 0 }
      query = tasks.joins(:treatment).where(treatments: {treatment_type: treatment_type, status: 'active'})
      data[treatment_type][:need] = query.count
      data[treatment_type][:completed] = query.where(is_completed: true).count
    end

    render json: { tasks: data }
  end

  def get_total_scores
    find_item
    authorize @item
    data = { total_score: @item.total_score, current_score: @item.current_score }
    render json: { scores: data }
  end

  def check_phn
    phn = params[:phn].to_i
    user = User.find(params[:id])

    render json: if user.PHN == phn
      user.update_attribute(:phn_validated, true)
      { response: true, message: '' }
    else
      { response: false, message: I18n.t('error.phn_dismatch') }
    end
  end

private

  def resource_class
    User
  end

  def order_params
    'full_name asc'
  end
end
