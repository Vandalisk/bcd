# encoding: utf-8

class AccountsController < ResourcesController

  def patients
    authorize self
    patients = User.patients.active.includes(:sent_treatments)
    result = patients.map do |patient|
      {
        id: patient.id,
        full_name: patient.full_name,
        doctor_id: User.doctors.pluck(:id).sample, #TODO add id of doctor, to who patient is assigned
        avatar: patient.gravatar_url,
        compliance_rate: ComplianceCalculator.calculate_compliance_for(patient),
        phn: SecureRandom.hex[0..10], #TODO replace with actual PHN
        date_of_birth: patient.birthday.strftime('%d/%m/%Y'),
        treatments: patient.sent_treatments.requested.map do |t|
          {
            date: t.created_at,
            source: rand > 0.5 ? 'DPD' : 'BCD' #TODO finish that part after DPD integration
          }
        end
      }
    end
    render json: result
  end

  def staff
    authorize self
    staff = User.staff.except_current(current_user.id)
    result = staff.map do |user|
      {
        id: user.id,
        full_name: user.full_name,
        avatar: user.gravatar_url,
        date_of_birth: user.birthday.strftime('%d/%m/%Y'),
        role: user.role
      }
    end
    render json: result
  end

private

  def resource_class
    User
  end

  def order_params
    'id desc'
  end
end
