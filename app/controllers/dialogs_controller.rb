# encoding: utf-8

class DialogsController < ResourcesController
  def index
    params[:self_id] = current_user.id
    @dialogs = Dialog.search(params)
    messages_ids = @dialogs.map(&:message_id).compact
    @messages = Message.where(id: messages_ids) rescue []
    render json: {
      dialogs: @dialogs.map{ |v| v.prepare_for_json },
      messages: @messages.map{ |v| v.prepare_for_json },
      dialogs_unread_count: Dialog.dialogs_unread_count(current_user.id),
    }
  end

  def dialogs_unread_count
    render json: {
      dialogs_unread_count: Dialog.dialogs_unread_count(current_user.id)
    }
  end
end
