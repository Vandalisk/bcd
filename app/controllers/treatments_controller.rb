# encoding: utf-8

class TreatmentsController < ResourcesController

  def index
    @treatments = TreatmentsControllerPolicy::Scope.new(current_user, Treatment).resolve_list
    @treatments = @treatments.includes(:feed_events, :treatment_template)

    @treatments = @treatments.search(params)
    @treatments = @treatments.order(order_params)
    @treatments = @treatments.paginate(pagination_params)
    render template: 'treatments/index'
  end

  def create
    # TODO maybe this action is unnecessary now
    authorize self
    @treatment = resource_scope.new(permitted_params)
    @treatment.sender_id = current_user.id
    @treatment.status = 'new'
    if @treatment.save
      data = view_context.render template: 'treatments/item', formats: :json
    else
      halt 422, I18n.t('error.validation') do |data|
        data[:errors] = @treatment.errors.full_messages
      end
    end
  end

  def update
    if block_given?
      super
    else
      treatment = Treatment.find(params[:id])
      old_status = treatment.status
      TreatmentsControllerPolicy::Scope.new(current_user, treatment).resolve_update

      treatment.assign_attributes(permitted_params)

      if ENV['RAILS_ENV'] != 'development'
        if treatment.main_condition_DPD && treatment.status_changed? && treatment.status_was != 'from_dpd'
          notifier = ToDPD::Notifier.new('dpd.treatment.prescribe')
        elsif treatment.main_condition_DPD && Treatment::NEEDED_FIELDS_FOR_DPD.any? { |argument| treatment.changed.include?(argument) }
          notifier = ToDPD::Notifier.new('dpd.treatment.update')
        elsif treatment.medication? && treatment.status_changed? && treatment.status_was == 'active'
          notifier = ToDPD::Notifier.new('dpd.treatment.delete')
        end
      end

      if treatment.valid?
        if notifier
          notifier.notify do
            treatment.save
            treatment
          end
        else
          treatment.save
        end
        feed_event = treatment.feed_events.last
        if !params[:status].blank? && params[:status] != old_status
          feed_event.update(sender_id: current_user.id)
        end
        @treatment = treatment
        data = view_context.render template: 'treatments/item', formats: :json
        unless ENV['RAILS_ENV'] == 'test'
          WsNotifier.notify_treatment(
            faye_client,
            @treatment.receiver_id,
            current_user,
            @treatment.status,
            [data]
          )
        end
        render json: data
      else
        halt 422, I18n.t('error.validation') do |data|
          data[:errors] = treatment.errors.full_messages
        end
      end
    end
  end

  def update_current
    find_item
    TreatmentsControllerPolicy::Scope.new(current_user, @item).resolve_update
    params.delete(:status)
    params.delete(:sender_id)

    copy_attributes = @item.attributes.merge(
      {
        'status' => 'declined',
        'doctor_comment' => params[:doctor_comment],
        'patient_comment' => params[:patient_comment],
        'data' => params[:data],
        'date_end' => Date.today
      }
    )
    copy_attributes.delete('id')


    Treatment.create(copy_attributes)
    # _debug params
    @item.sender_id = current_user.id
    @item.status = current_user.role == 'patient' ? 'requested' : 'new'
    @item.patient_comment = params[:patient_comment]
    @item.description = params[:description]
    if @item.update(permitted_params)
      @item.tasks.where('date > ?', Date.today).delete_all
      @treatment = @item
      data = view_context.render template: 'treatments/item', formats: :json
      unless ENV['RAILS_ENV'] == 'test'
        WsNotifier.notify_treatment(
          faye_client,
          @treatment.receiver_id,
          current_user,
          @treatment.status,
          [data]
        )
      end
      render json: data
    else
      halt 422, I18n.t('error.validation') do |data|
        data[:errors] = @item.errors.full_messages
      end
    end
  end

  def submit_treatments
    treatment_names = []
    treatments = []
    begin
      receiver = User.find(params[:items][0]['receiver_id'])
    rescue
      halt 422 and return
    end
    ids = params[:items].map { |item| item['id'].to_i }
    templates = TreatmentsControllerPolicy::Scope.new(current_user,
                                                      TreatmentTemplate,receiver).
                                                      resolve_templates.where(id: ids)
    if params[:items].is_a?(Array)
      params[:items].each do |treatment|
        tpl = templates.detect { |template| template.id == treatment['id'].to_i }
        if tpl
          treatment = Treatment.new({
            status: User::ACCESS[:doctor].include?(current_user.role) ? 'new' : 'requested',
            sender_id: current_user.id,
            receiver_id: User::ACCESS[:doctor].include?(current_user.role) ? treatment['receiver_id'] : current_user.id,
            treatment_template_id: treatment['id'],
            patient_comment: User::ACCESS[:doctor].include?(current_user.role) ? nil : treatment['patient_comment'],
            data: treatment[:data],
            frequency: treatment[:frequency],
            period: treatment[:open_ended] ? false : treatment[:period],
            open_ended: treatment[:open_ended],
            description: treatment[:description] || tpl.description
          })
          [:treatment_type, :name, :full_name].each do |field|
            treatment.send(:"#{field}=", tpl.send(field))
          end
          if treatment.save
            treatment_names << treatment.name
            @treatment = treatment
            treatments << view_context.render(template: 'treatments/item', formats: :json)
          else
            halt 422, I18n.t('error.validation') do |data|
              data[:errors] = treatment.errors.full_messages
            end
          end
        end
      end
    end

    data = {}
    data[:sender_id] = current_user.id
    data[:receiver_id] = User::ACCESS[:doctor].include?(current_user.role) ? params[:receiver_id] : current_user.id
    data[:data] = {}
    data[:data][:event] = 'treatment_request'
    data[:data][:treatments_names] = treatment_names
    # TODO: use params[:items] for fill comments fields. Use iterator
    FeedEvent.create(data)
    unless ENV['RAILS_ENV'] == 'test'
      WsNotifier.notify_treatment(
        faye_client,
        @treatment.receiver_id,
        current_user,
        User::ACCESS[:doctor].include?(current_user.role) ? 'suggested' : 'requested',
        treatments
      )
    end
    render nothing: true
  end

  def prescribe_requested_treatment
    authorize self
    find_item
    params.delete(:status)
    params.delete(:sender_id)
    # _debug params
    if @item.update(permitted_params.merge({sender_id: current_user.id, status: 'new'}))
      @treatment = @item
      data = view_context.render template: 'treatments/item', formats: :json
      unless ENV['RAILS_ENV'] == 'test'
        WsNotifier.notify_treatment(
          faye_client,
          @treatment.receiver_id,
          current_user,
          @treatment.status,
          [data]
        )
      end
      render json: data
    else
      halt 422, I18n.t('error.validation') do |data|
        data[:errors] = @item.errors.full_messages
      end
    end
  end

  def destroy
    TreatmentsControllerPolicy::Scope.new(current_user, @item).resolve_update
    @treatment = @item
    @item.destroy
    data = view_context.render template: 'treatments/item', formats: :json
    unless ENV['RAILS_ENV'] == 'test'
      WsNotifier.notify(
        faye_client,
        @treatment.receiver_id,
        current_user,
        'treatment',
        'deleted',
        [data]
      )
    end
    render json: data
  end

  private

  def resource_class
    Treatment
  end

  def order_params
    'date_start desc'
  end

  def permitted_params
    params.permit(*resource_class::FIELDS_FOR_FORMS).tap do |whitelisted|
      whitelisted[:data] = params[:data] if params[:data]
    end
  end
end
