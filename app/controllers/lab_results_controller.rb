class LabResultsController < ApplicationController
  def get_from_dpd
    render json: LabResultsFetcher.new.fetch(params[:request_params])
  end
end
