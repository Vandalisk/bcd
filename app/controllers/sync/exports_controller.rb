# encoding: utf-8

class Sync::ExportsController < ResourcesController
  skip_before_action :require_user, only: :download

  def index
  end

  def create
    options = {}
    options[:what]      = params[:what]
    options[:date_from] = params[:date_from] unless params[:date_from].blank?
    options[:date_to]   = params[:date_to] unless params[:date_to].blank?

    item = resource_class.create({ options: options, user_id: current_user.id })

    render json: {
      success: true,
      url: url_for(action: :to_csv, id: item),
    }
  end

  def to_csv
    item = resource_class.find(params[:id])

    item.export_to_csv!

    result = {
      success: true,
      total_count: item.total_count,
      done_count: item.done_count,
    }
    result[:url] = url_for(action: :download, id: item) if item.status == 'done'

    render json: result
  end

  def download
    item = resource_class.find(params[:id])
    send_file item.file, type: 'text/csv'
  end


private

  def resource_class
    ::Sync::Export
  end

end
