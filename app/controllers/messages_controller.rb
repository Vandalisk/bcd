# encoding: utf-8

class MessagesController < ResourcesController

  def index
    params[:self_id] = current_user.id
    super do |s|
      s = s.includes(:sender, :receiver)
      s
    end
  end

  def create
    @item = resource_scope.new(permitted_params)
    @item.sender_id = current_user.id
    if @item.save
      render json: {
        new_item: @item.prepare_for_json,
        dialogs_unread_count: Dialog.dialogs_unread_count(current_user.id)
      }, status: 201
      faye_client.publish "/messages/#{@item.receiver_id}", {
        new_item: @item.prepare_for_json,
          dialogs_unread_count: Dialog.dialogs_unread_count(@item.receiver_id)
      }
    else
      halt 422, I18n.t('error.validation') do |data|
        data[:errors] = @item.errors.full_messages
      end
    end
  end

  def read_multiple
    Message.where(id: params[:message_ids]).update_all(is_read: true)
    message = Message.where(id: params[:message_ids]).first
    sender_id = message.sender_id
    receiver_id = message.receiver_id
    render json: {
      updated_item_ids: params[:message_ids],
      dialogs_unread_count: Dialog.dialogs_unread_count(current_user.id)
    }
    faye_client.publish "/messages/#{sender_id}", {
      updated_item_ids: params[:message_ids],
        dialogs_unread_count: Dialog.dialogs_unread_count(sender_id)
    }
    faye_client.publish "/messages/#{receiver_id}", {
      updated_item_ids: params[:message_ids],
        dialogs_unread_count: Dialog.dialogs_unread_count(receiver_id)
    }
  end

private

  def resource_class
    ::Message
  end

  def order_params
    params[:order] || 'id desc'
  end

end
