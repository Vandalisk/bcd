# encoding: utf-8

class AuthController < ApplicationController
  skip_before_action :require_user, only: [:login, :send_reset_password_link, :find_by_token, :update_password]

  def login
    @item = User.find_by(email: params[:email])
    if @item && @item.authenticate(params[:password])
      if @item.class::ACCESS[:doctor].include?(@item.role) || @item.patient? && @item.active?
        render json: { item: response_with(@item) }
      else
        halt 403, I18n.t('error.removed_from_DPD_program')
      end
    else
      halt :unauthorized, I18n.t('error.wrong_email_password') #401
    end
  end

  def logout
    # TODO: remove token or mark it as expired
    render json: { item: current_user.prepare_for_json }
  end

  def profile
    render json: { item: current_user.prepare_for_json }
  end

  def profile_submit
    if current_user.update(permitted_params)
      render json: { item: current_user.prepare_for_json }
    else
      halt 422, I18n.t('error.validation') do |data|
        data[:errors] = current_user.errors.full_messages
      end
    end
  end

  def send_reset_password_link
    @user = User.where(email: params[:email]).first
    halt 404, I18n.t('.error.no_users_with_such_email') and return unless @user
    @token = @user.generate_token!('reset_password')
    UserMailer.reset_password(user: @user, token: @token).deliver_later
    render json: { message: 'ok' }
  end

  def find_by_token
    @user = User.find_by_token(params[:token], params[:token_type])
    begin
      render json: { item: @user.prepare_for_json }
    rescue
      halt 404, I18n.t('.errors.no_users_with_such_token')
    end
  end

  def update_password
    @user = User.find_by_token(params[:token], params[:token_type])

    halt 404, I18n.t('error.not_found') and return unless @user
    halt 422, 'Please, set new password' and return unless params[:password].to_s.size > 0
    halt 403, I18n.t('error.forbidden') and return unless @user.phn_validated

    @user.password = params[:password]
    @user.save(validate: false)

    @user.tokens.where(token_type: 'access').delete_all
    @user.tokens.where(token: params[:token]).delete_all #invalidate current token

    token = @user.generate_token!('access')
    attrs = @user.prepare_for_json
    attrs['token'] = token.token

    render json: { item: attrs } rescue nil
  end

private
  def response_with(item)
    token = item.tokens.where(token_type: 'access').first || item.generate_token!('access')
    attrs = item.prepare_for_json
    attrs['token'] = token.token
    attrs
  end

  def permitted_params
    params.permit(*User::FIELDS_FOR_FORMS)
  end
end
