# encoding: utf-8

class BloodGlucosesController < ResourcesController
  before_action :authorize_measurement, only: [:update, :destroy]

  def index
    measurement = Measure.new(permitted_params)
    authorize measurement
    @measurements = Measure.search(params).group_by(&:date).map do |key, objects|
      {
        date: key,
        measurements: objects.as_json(only: [:id, :when, :value])
      }
    end
    render json: @measurements
  end

  def create
    @measurement = resource_scope.new(permitted_params)
    authorize @measurement
    if @measurement.save
      render json: @measurement.prepare_for_json, status: 201
      unless ENV['RAILS_ENV'] == 'test'
        WsNotifier.notify_blood_glucose(faye_client, @measurement.patient_id, current_user, 'created', @measurement.prepare_for_json)
      end
    else
      halt 422, I18n.t('error.validation') do |data|
        data[:errors] = @measurement.errors.full_messages
      end
    end
  end

  def update
    if @measurement.update(permitted_params)
      render json: @measurement.prepare_for_json, status: 201
      unless ENV['RAILS_ENV'] == 'test'
        WsNotifier.notify_blood_glucose(faye_client, @measurement.patient_id, current_user, 'updated', @measurement.prepare_for_json)
      end
    else
      halt 422, I18n.t('error.validation') do |data|
        data[:errors] = @measurement.errors.full_messages
      end
    end
  end

  private

  def authorize_measurement
    @measurement = Measure.find(params[:id])
    authorize @measurement
  end

  def resource_class
    Measure
  end
end
