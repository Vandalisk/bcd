# encoding: utf-8

class Task < ActiveRecord::Base
  include TreatmentBase

  ATTRIBUTES = [:treatment_id, :date, :is_completed, :data_completed, :comment].freeze

  auto_strip_attributes(*ATTRIBUTES)

  serialize :data_completed, JSON

  belongs_to :treatment

  has_one :doctor, through: :treatment
  has_one :patient, through: :treatment

  after_update :update_treatment_completion

  scope :completed, -> { where(is_completed: true) }

  trigger.after(:update).of(:is_completed) do
    <<-SQL
      IF NEW.is_completed THEN
        UPDATE users SET current_score = current_score + NEW.points FROM treatments
        WHERE treatments.receiver_id = users.id AND treatments.id = NEW.treatment_id;
      ELSE
        UPDATE users SET current_score = current_score - NEW.points FROM treatments
        WHERE treatments.receiver_id = users.id AND treatments.id = NEW.treatment_id;
      END IF
    SQL
  end

  def prepare_for_json
    self.attributes.merge(treatment_type: self.treatment.treatment_type, name: self.treatment.name)
  end

  def update_treatment_completion
    completion_percent = calculate_completion
    compliance_rate = ComplianceCalculator.calculate_compliance_for(self)

    status = completion_percent == 100 ? 'completed' : 'active'

    self.treatment.update(
      status: status,
      completion_percent: completion_percent,
      compliance_rate: compliance_rate
    )
  end

  def set_points!
    update_attributes(
      points: PowerPointCalculator.calculate(
        treatment.treatment_template.power,
        treatment.frequency
      )
    )
  end

  def calculate_completion
    all_tasks_count = self.treatment.tasks.count
    completed_tasks_count = self.treatment.tasks.where(is_completed: true).count
    if completed_tasks_count == 0
      0
    else
      completed_tasks_count.to_f / all_tasks_count.to_f * 100
    end
  end

  def complete!
    update_attribute(:is_completed, true)
  end

  def self.search(params = {})
    s = self.all
    [:treatment_id, :date, :is_completed].each do |field|
      s = s.where(:"#{field}" => params[field]) if params[field].is_a?(String) && !params[field].blank?
    end
    s
  end

  def self.export_query(params = {})
    s = all
    # s = s.joins("LEFT JOIN treatments ON treatments.id = tasks.treatment_id")
    # s = s.select("tasks.*, treatments.name AS name, treatments.treatment_type AS treatment_type")
    s = s.includes(:treatment => :patient)
    s = s.where(["date <= ?", Date.today])
    s
  end

  def self.prepare_export_items(s)
    items = []
    s.each do |rec|
      item = {}
      item[:id] = rec.id
      item["User"] = rec.treatment.patient.full_name
      item["Date"] = rec.date
      item["Treatment name"] = rec.treatment.name
      # item["Prescribed"] = rec.treatment.full_name
      item["Prescribed"] = ("#{rec.treatment.class.condition_text(rec.treatment.treatment_type, rec.treatment.data)}" rescue nil)
      item["Confirmed"] = ("#{rec.class.condition_text(rec.treatment.treatment_type, rec.data_completed)}" rescue nil)
      item["Comment"] = rec.comment

      items << item
    end
    items
  end

  FIELDS_FOR_FORMS  = ATTRIBUTES.freeze
end
