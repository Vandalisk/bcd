# encoding: utf-8
require 'digest/md5'

class User < ActiveRecord::Base
  attr_readonly :PHN
  ATTRIBUTES = [:email, :role, :phone, :first_name, :last_name, :birthday, :gender].freeze

  auto_strip_attributes(*ATTRIBUTES)

  has_many :tokens, dependent: :destroy
  has_many :sent_treatments, class_name: 'Treatment', foreign_key: :sender_id, dependent: :destroy
  has_many :received_treatments, class_name: 'Treatment', foreign_key: :receiver_id, dependent: :destroy

  has_many :tasks, through: :received_treatments

  validates :email, :first_name, :last_name, :birthday, :gender, :role,
    presence: true

  validates :id, format: { with: /\A\d+\Z/,  message: "must be a number", unless: "id.blank?" }

  validates :email,
    presence: true,
    uniqueness: { unless: 'email.blank?' },
    format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, unless: 'email.blank?' }

  after_save :set_full_name
  after_save :set_gravatar_url

  scope :patients, -> { where(role: 'patient') }
  scope :doctors, -> { where(role: 'doctor') }
  scope :active, -> { where(active: true) }
  scope :case_managers, -> { where(role: 'case_manager') }
  scope :staff, -> { where(role: ['doctor', 'case_manager']) }
  scope :except_current, -> (id) { where.not(id: id) }

  attr_reader :password

  ROLES = %w(admin doctor case_manager patient)

  ACCESS = {
    private: ['patient', 'doctor', 'admin', 'case_manager'],
    patient: ['patient'],
    doctor:  ['doctor', 'admin', 'case_manager'],
    head:    ['doctor', 'admin']
  }

  def role
    ActiveSupport::StringInquirer.new(read_attribute(:role))
  end

  def authenticate(unencrypted_password)
    return false if password_digest.blank?
    # ::BCrypt::Password.new(password_digest) == unencrypted_password && self
    secure_compare(::BCrypt::Engine.hash_secret(unencrypted_password, ::BCrypt::Password.new(password_digest).salt), password_digest) && self
  end

  def password=(unencrypted_password)
    unless unencrypted_password.blank?
      @password = unencrypted_password
      self.password_digest = ::BCrypt::Password.create(unencrypted_password, cost: BCrypt::Engine::DEFAULT_COST)
    end
  end

  def generate_token!(type)
    self.tokens.create(token: SecureRandom.hex(32), token_type: type)
  rescue ActiveRecord::RecordNotUnique
    retry
  end

  def unsubscribe!
    update_column(:active, false)
  end

  def to_json(options = {})
    options[:except] ||= [:password_digest]
    super options
  end

  def prepare_for_json
    a = self.attributes.except('password_digest', 'PHN')
    a['gravatar_url'] = self.gravatar_url
    a
  end

  def set_scores!
    self.update_columns(
      current_score: Task.joins(:treatment).where(is_completed: true, treatments: {status: 'active', receiver_id: self.id}).sum(:points),
      total_score: self.received_treatments.active.sum(:scores)
    )
  end

  def set_full_name
    self.update_columns(full_name: [first_name, last_name].delete_if{ |v| v.blank? }.join(" "))
  end

  def set_gravatar_url
    self.update_columns(gravatar_url: "http://www.gravatar.com/avatar/#{Digest::MD5.hexdigest(email.downcase)}?d=mm")
  end

  def self.find_by_token(token, token_type)
    Token.where(token: token, token_type: token_type).first.user rescue nil
  end

  def self.search(params = {})
    s = self.all
    s = s.where("#{quoted_table_name}.role IN (?)", params[:role].split(',').map(&:strip)) if params[:role]
    s = s.where("#{quoted_table_name}.id NOT IN (?)", params[:id_not].split(',').map(&:strip)) if params[:id_not]
    self::FIELDS_FOR_SEARCH.each do |field|
      s = s.where(:"#{field}" => params[field]) if params[field].is_a?(String) && !params[field].blank?
    end
    if params[:q].is_a?(String)
      q = []
      self::FIELDS_FOR_SEARCH.each { |field| q << "#{quoted_table_name}.#{field} ILIKE :q" }
      s = s.where([q.join(" OR "), { q: "%#{params[:q]}%" }])
      s = yield(s) if block_given?
    end
    s
  end

  ROLES.each do |certain_role|
    define_method("#{certain_role}?") do
      role == certain_role
    end
  end

  FIELDS_FOR_SEARCH = %i(full_name phone email).freeze
  FIELDS_FOR_FORMS  = %i(role email phone first_name last_name birthday gender password).freeze
  FIELDS_FOR_RABBIT = %w(email phone first_name last_name birthday gender id PHN version)
  FIELDS_FOR_UPDATE = %w(email phone first_name last_name birthday gender id version)

  def prepare_for_DPD
    data = attributes.select { |key, value| FIELDS_FOR_UPDATE.include? (key) }
    data["birthday"] = data["birthday"].strftime
    data
  end
end
