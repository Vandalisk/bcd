# encoding: utf-8

class Treatment < ActiveRecord::Base
  include TreatmentBase
  include AASM

  ATTRIBUTES = [:sender_id, :receiver_id, :treatment_template_id, :treatment_type, :name, :data, :frequency, :open_ended, :period, :date_start, :date_end, :description, :status, :completion_percent, :full_name].freeze

  serialize :data, JSON

  auto_strip_attributes :sender_id, :receiver_id, :name, :data, :frequency, :open_ended, :period, :date_start, :date_end, :status, :completion_percent, :full_name
  auto_strip_attributes :description, squish: false

  belongs_to :doctor, class_name: "User", foreign_key: :sender_id
  belongs_to :patient, class_name: "User", foreign_key: :receiver_id

  belongs_to :treatment_template, class_name: "TreatmentTemplate", foreign_key: :treatment_template_id
  has_many :tasks, dependent: :destroy
  has_many :feed_events, as: :resource

  validates :name, presence: true
  validates :dpd_id, uniqueness: true, allow_nil: true

  after_save :to_current_callback, if: -> { status_changed? && active? && ['new', nil].include?(status_was) }
  after_create :to_current_callback, if: -> { active? }

  after_save :set_full_name, if: -> { treatment_template }
  after_save :save_event_to_feed

  delegate :full_name, to: :doctor, prefix: true
  delegate :full_name, to: :treatment_template, prefix: true
  delegate :full_name, to: :patient, prefix: true

  delegate :impact, :insurance, :power, to: :treatment_template

  delegate :gravatar_url, to: :doctor, prefix: :sender
  delegate :gravatar_url, to: :patient, prefix: :receiver

  scope :desc_ordered, -> { order('created_at DESC') }
  scope :outdated, -> { active.where('date_end < ?', Date.today) }

  aasm column: :status do
    state :new, initial: true
    state :requested
    state :from_dpd
    state :active
    state :declined
    state :stopped
    state :completed

    event :move_to_current do
      transitions from: :new, to: :active
    end

    event :stop do
      transitions from: :active, to: :stopped
    end

    event :decline do
      transitions from: :requested, to: :stopped
    end

    event :cancel do
      transitions from: :new, to: :stopped
    end

    event :complete do
      transitions from: :active, to: :completed
    end
  end

  STATUSES = %i(requested new active declined stopped completed from_dpd).freeze

  trigger.after(:update) do
    <<-SQL
      IF OLD.status = 'active' AND NEW.status <> 'active' THEN
        UPDATE users SET total_score = total_score - NEW.scores,
                         current_score = current_score - round(NEW.scores *
                            cast((SELECT COUNT(*) FROM tasks WHERE tasks.treatment_id = NEW.id AND tasks.is_completed = true) as float)
                            /
                            (SELECT COUNT(*) FROM tasks WHERE tasks.treatment_id = NEW.id)
                         )
                         WHERE users.id = NEW.receiver_id;
      ELSIF NEW.status = 'active' AND OLD.status <>'active' THEN
        UPDATE users SET total_score = total_score + NEW.scores WHERE users.id = NEW.receiver_id;
      END IF
    SQL
  end

  def decline!
    update_columns(status: 'declined', dpd_id: nil)
  end

  def current?
    requested? || new? || active?
  end

  def main_condition_DPD
    medication? && active?
  end

  def prepare_for_json
    self.attributes
  end

  def medication?
    treatment_type == 'medication_tablets' || treatment_type == 'medication_injection'
  end

  def frequency_name
    TreatmentBase::FREQUENCY_OPTIONS.select{ |rec| rec[:key] == self.frequency }[0][:name] rescue ''
  end

  def set_full_name
    full_name = "#{self.class.condition_text(self.treatment_type, self.data)} #{frequency_name}"
    full_name += " for #{self.period} days" unless self.open_ended
    self.update_columns(full_name: full_name)
  end

  def to_current_callback
    date_start = Date.tomorrow
    date_end = date_start + (self.open_ended ? 6.month : self.period)

    self.transaction do
      self.update_columns(date_start: date_start, date_end: date_end)
      dates = self.class.get_task_dates(date_start, date_end, frequency)
      dates.each do |date|
        tasks.create(
          date: date,
          is_completed: false,
          points: PowerPointCalculator.calculate(treatment_template.power, frequency)
        )
      end
      set_scores!
    end
  end

  def save_event_to_feed
    # _debug "SAVE_EVENT_TO_FEED"
    if self.status_changed?
      options = {
        resource_type: self.class.to_s,
        resource_id: self.id,
      }
      options[:data] = {}
      options[:data][:event] = "suggest"    if self.status == "new"
      options[:data][:event] = "accept"     if self.status == "active" && ["new", "declined"].include?(self.status_was)
      options[:data][:event] = "reactivate" if self.status == "active" && ["stopped"].include?(self.status_was)
      options[:data][:event] = "decline"    if self.status == "declined"
      options[:data][:event] = "stop"       if self.status == "stopped"
      options[:data][:event] = "complete"   if self.status == "completed"

      if options[:data][:event]
        options[:sender_id] = options[:data][:event] == "suggest" ? self.sender_id : self.receiver_id
        #options[:sender_id] = self.receiver_id if self.status == "declined"
        # не придумал пока нормального способа отслеживать, какой юзер вызвал эвент
        options[:receiver_id] = self.receiver_id
        FeedEvent.create(options)
      end
    end
  end

  def self.search(params = {})
    s = self.all
    [:sender_id, :receiver_id, :full_name, :status].each do |field|
      s = s.where(:"#{field}" => params[field]) if params[field].is_a?(String) && !params[field].blank?
    end
    if params[:q].is_a?(String)
      q = []
      self::FIELDS_FOR_SEARCH.each { |field| q << "#{quoted_table_name}.#{field} ILIKE :q" }
      s = s.where([q.join(" OR "), { q: "%#{params[:q]}%" }])
      s = yield(s) if block_given?
    end
    s
  end

  def self.get_task_dates(date_start, date_end, frequency)
    hash = {
      'daily': 1.day,
      'every_other_day': 2.days,
      '6_days_per_week': [1,2,3,4,5,6],
      '5_days_per_week': [1,2,3,4,5],
      '4_days_per_week': [0,1,3,5],
      '3_days_per_week': [1,3,5],
      '2_days_per_week': [2,4],
      'weekly': 1.week,
      'twice_monthly': 2.weeks,
      'monthly': 1.month
    }
    key = hash[:"#{frequency}"]
    if key.kind_of?(Array)
      date_start.step(date_end).select{ |d| d.wday.in?(key) }
    else
      (date_start.to_datetime.to_i..date_end.to_datetime.to_i).step(key).map{ |d| Time.at(d).to_date }
    end
  end

  def aasm_state
    status
  end

  def set_scores!
    scores = PowerPointCalculator.calculate(
      treatment_template.power,
      frequency,
      self.class.get_task_dates(date_start, date_end, frequency).count
    )
    update_columns(scores: scores)
    patient.set_scores! if patient
  end

  NEEDED_FIELDS_FOR_DPD = %w(name data description sender_id receiver_id period version)

  FIELDS_FOR_SEARCH = %i(full_name).freeze

  FIELDS_FOR_FORMS = %i(
    sender_id
    receiver_id
    treatment_template_id
    treatment_type
    name
    data
    frequency
    period
    description
    status
    completion_percent
    doctor_comment
    patient_comment
    open_ended
  ).freeze

  FIELDS_FOR_RABBIT = %w(
    id
    sender_id
    receiver_id
    name
    date_start
    dpd_id
    dpd_dosage
    date_end
    doctor_comment
    data
    frequency
    period
    description
    version
    dpd_id
  )

  def prepare_for_DPD
    data = attributes.select { |key, value| FIELDS_FOR_RABBIT.include? (key) }
    data["date_start"] = data["date_start"].strftime
    data["date_end"] = data["date_end"].strftime
    data["patient_id"] = data.delete("receiver_id")
    data["doctor_id"] = data.delete("sender_id")
    data
  end

  def suggest_or_request
    feed_events.where('data ILIKE ? OR data ILIKE ?', '%suggest%', '%request%').try(:first).try(:data).try(:[], 'event')
  end

  def accepted_by_full_name
    feed_events.where('data ILIKE ?', "%accept%").last.try(:sender).try(:full_name)
  end

  def declined_by_full_name
    if self.active?
      self.try(:doctor_full_name)
    else
      self.feed_events.where('data ILIKE ?', "%decline%").first.try(:sender).try(:full_name)
    end
  end

  def declined_at
    feed_events.where('data ILIKE ?', "%decline%").first.try(:created_at)
  end
end
