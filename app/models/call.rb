class Call < ActiveRecord::Base
  belongs_to :caller, class_name: 'User'
  belongs_to :recipient, class_name: 'User'
  belongs_to :room
end
