module ActiveRecordChangers::TransactionSkipper
  def remove_active_record_transaction_wrapper
    ActiveRecord::ConnectionAdapters::PostgreSQLAdapter.class_eval do
      def custom_begin_db_transaction; end
      def custom_commit_db_transaction; end
      def begin_backup; end
      def commit_backup; end

      alias_method :begin_backup, :begin_db_transaction
      alias_method :commit_backup, :commit_db_transaction
      alias_method :begin_db_transaction, :custom_begin_db_transaction
      alias_method :commit_db_transaction, :custom_commit_db_transaction
    end
  end

  def return_active_record_transaction_wrapper
    ActiveRecord::ConnectionAdapters::PostgreSQLAdapter.class_eval do
      alias_method :begin_db_transaction, :begin_backup
      alias_method :commit_db_transaction, :commit_backup

      remove_method :custom_begin_db_transaction, :custom_commit_db_transaction, :begin_backup, :commit_backup
    end
  end
end
