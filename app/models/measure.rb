class Measure < ActiveRecord::Base

  belongs_to :patient, class_name: 'User', foreign_key: :patient_id

  validates :value, numericality: true
  validates_presence_of :value, :date

  MEASURE_WHEN_TYPES =
  %w(
    early_hours
    before_breakfast
    after_breakfast
    before_lunch
    after_lunch
    before_dinner
    after_dinner
    before_bed
  )

  ATTRIBUTES = [:value, :when, :date, :patient_id].freeze

  scope :by_date, -> (start_date, end_date) { where('DATE(date) BETWEEN ? AND ?', start_date, end_date) }
  scope :by_patient, -> (patient_id) { where(patient_id: patient_id) if patient_id }
  scope :by_value, -> (value) { where(value: value) if value }

  def self.search(params = {})
    date = MeasurePresenter.new(self).create_date(params)
    by_date(date, date.end_of_month).by_patient(params[:patient_id]).by_value(params[:value])
  end

  def self.policy_class
    BloodGlucosePolicy
  end

  def prepare_for_json
    attributes
  end

  FIELDS_FOR_FORMS = ATTRIBUTES.freeze
end
