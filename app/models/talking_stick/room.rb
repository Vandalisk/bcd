module TalkingStick
  class Room < ActiveRecord::Base
    has_many :participants, dependent: :destroy
    has_many :signals, dependent: :destroy
    has_one :call

    before_validation :sluggify_name
    after_create :create_call

    def self.find_or_create(slug:)
      slug = slug.parameterize
      find_by(slug: slug) || create(name: slug.titleize, slug: slug)
    end

    def to_param
      slug
    end

    private

    def sluggify_name
      self.slug = name.parameterize unless slug.present?
    end

    def create_call
      Call.create(room_id: self.id)
    end

    trigger.before(:delete) do
      <<-SQL
        UPDATE calls SET ended_at = now(),
                         duration = age(OLD.last_used::timestamp, started_at::timestamp)
                         WHERE calls.room_id = OLD.id;
      SQL
    end
  end
end
