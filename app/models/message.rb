# encoding: utf-8

class Message < ActiveRecord::Base
  ATTRIBUTES = [:sender_id, :receiver_id, :text, :is_last, :is_read, :message_type].freeze

  auto_strip_attributes(*ATTRIBUTES)
  validates_presence_of :text
  validates :sender, :receiver, presence: true

  belongs_to :sender, class_name: "User", foreign_key: :sender_id
  belongs_to :receiver, class_name: "User", foreign_key: :receiver_id

  after_commit :set_last

  scope :prepare_for_rabbit, -> (params) {
    by_patients(params["patient_ids"]).by_doctor(params["doctor_id"]).by_date(params["date_from"], params["date_to"]).select(:id, :sender_id, :receiver_id, :text, :created_at)
  }
  scope :by_date, -> (start_date, end_date) do
    start_date ||= Date.new
    end_date ||= Date.today
    where("DATE(created_at) BETWEEN ? AND ?", start_date, end_date)
  end

  scope :by_patients, -> (ids) { where("receiver_id IN (?) OR sender_id IN (?)", ids, ids) }
  scope :by_doctor, -> (doctor_id) { where("receiver_id = ? OR sender_id = ?", doctor_id, doctor_id)}

  def set_last
    self.class.transaction do
      s = self.class.where("(sender_id = :sender_id AND receiver_id = :receiver_id) OR (sender_id = :receiver_id AND receiver_id = :sender_id)",
                           sender_id: self.sender_id,
                           receiver_id: self.receiver_id)
      s.update_all(is_last: false)
      s.last.update_columns(is_last: true) rescue nil
    end
  end

  def prepare_for_json
    a = self.attributes
    a["sender_full_name"] = self.sender.full_name rescue nil
    a["sender_gravatar_url"] = self.sender.gravatar_url rescue nil
    a
  end

  def self.search(params = {})
    s = self.all

    return s.where('FALSE') if !params[:self_id] || !params[:dialog_with]

    s = s.where("(sender_id = :self_id AND receiver_id = :other_user_id) OR (sender_id = :other_user_id AND receiver_id = :self_id)", self_id: params[:self_id], other_user_id: params[:dialog_with])
    s = s.where("id > ?", params[:id_gt]) if params[:id_gt]
    s = s.where("id < ?", params[:id_lt]) if params[:id_lt]
    s
  end

  FIELDS_FOR_FORMS = ATTRIBUTES.freeze
  FIELDS_FOR_RABBIT = %w(patient_ids date_from date_to doctor_id)
end
