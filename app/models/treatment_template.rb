# encoding: utf-8

class TreatmentTemplate < ActiveRecord::Base
  include TreatmentBase

  ATTRIBUTES = [:author_id, :treatment_type, :name, :data, :frequency, :period, :description, :full_name, :power].freeze

  serialize :data, JSON

  auto_strip_attributes :author_id, :treatment_type, :name, :frequency, :period, :full_name
  auto_strip_attributes :description, squish: false

  # has_many :treatments, dependent: :destroy
  belongs_to :author, foreign_key: :author_id, class_name: "User"
  belongs_to :editor, foreign_key: :editor_id, class_name: "User"

  validates :author_id, :treatment_type, :name, :frequency, :insurance, :power,
    presence: true
  validates :name, uniqueness: true

  after_save :set_full_name

  def prepare_for_json
    a = self.attributes
    a["author_name"] = self.author.full_name rescue nil
    a["editor_name"] = self.editor.full_name rescue nil
    a["points_per_task"] = PowerPointCalculator.calculate(self.power, self.frequency, 1)
    a
  end

  def frequency_name
    TreatmentBase::FREQUENCY_OPTIONS.select{ |rec| rec[:key] == self.frequency }[0][:name] rescue ""
  end

  def set_full_name
    # _debug "SET_FULL_NAME"
    full_name = "#{self.class.condition_text(self.treatment_type, self.data)} #{frequency_name}"
    if self.open_ended
      full_name << ", open-ended"
    else
      full_name << ", for #{self.period} days"
    end
    self.update_columns(full_name: full_name)
  end

  def self.search(params = {})
    s = self.all
    [:author_id, :treatment_type, :full_name].each do |field|
      s = s.where(:"#{field}" => params[field]) if params[field].is_a?(String) && !params[field].blank?
    end
    if params[:q].is_a?(String)
      q = []
      self::FIELDS_FOR_SEARCH.each { |field| q << "#{quoted_table_name}.#{field} ILIKE :q" }
      s = s.where([q.join(" OR "), { q: "%#{params[:q]}%" }])
      s = yield(s) if block_given?
    end
    s
  end

  FIELDS_FOR_SEARCH = [:full_name].freeze
  FIELDS_FOR_FORMS  = [:author_id, :impact, :insurance, :treatment_type, :name, :frequency, :period, :description, :full_name, :power, :open_ended].freeze
end
