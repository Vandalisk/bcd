# encoding: utf-8

module FayeExt
  class ClientToken
    def initialize(current_access_token = nil)
      @current_access_token = current_access_token
    end
    def outgoing(message, callback)
      message["ext"] ||= {}
      message["ext"]["auth_token"] = @current_access_token
      callback.call(message)
    end
  end
end
