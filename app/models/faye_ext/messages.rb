# encoding: utf-8

# Channel: /messages/123
module FayeExt
  class Messages
    PATTERN = /\A\/messages\/(\d+)\Z/

    def incoming(message, callback)
      # auth subscription
      if message["channel"] == "/meta/subscribe" && message["subscription"] =~ PATTERN
        m = PATTERN.match(message["subscription"])
        message["error"] = "403::Authentication required" unless message["ext"]["user_id"] && m[1].to_i == message["ext"]["user_id"]
      end

      # auth publishing
      if message["channel"] =~ PATTERN
        message["error"] = "403::Authentication required" unless message["ext"]["user_id"]
      end


      # _debug message
      callback.call(message)
    end

  end
end
