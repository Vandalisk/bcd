# encoding: utf-8

module FayeExt
  class Auth
    def incoming(message, callback)
      return callback.call(message) if message["channel"] =~ /\A\/meta\// && message["channel"] != "/meta/subscribe"
      message['ext'] ||= {}
      token = message['ext']['auth_token']
      user_id = User.find_by_token(token, 'access').id rescue nil if token
      message['ext'].delete('auth_token')
      message['ext'].delete('user_id')
      message['ext']['user_id'] = user_id if user_id
      callback.call(message)
    end
  end
end
