# encoding: utf-8

module FayeExt
  class ClearExt
    def outgoing(message, callback)
      message.delete("ext")
      callback.call(message)
    end
  end
end
