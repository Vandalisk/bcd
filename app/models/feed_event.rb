# encoding: utf-8

class FeedEvent < ActiveRecord::Base
  ATTRIBUTES = [:resource_type, :resource_id, :sender_id, :receiver_id, :data].freeze

  auto_strip_attributes(*ATTRIBUTES)

  serialize :data, JSON

  belongs_to :sender, class_name: 'User'
  belongs_to :receiver, class_name: 'User'
  belongs_to :resource, polymorphic: true

  def prepare_for_json
    self.attributes
  end

  def self.search(params = {})
    self.all
  end

  FIELDS_FOR_FORMS = ATTRIBUTES.freeze
end
