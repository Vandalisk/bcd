# encoding: utf-8

module TreatmentBase
  extend ActiveSupport::Concern

  TREATMENT_TYPES = [
    "medication",
    "exercise",
    "lifestyle",
  ].freeze

  TREATMENT_UNITS = [
    {key: 'mg tabs', name: 'mg', unit: 'mg tabs', action: 'take', thing: 'tabs'},
    {key: 'micrograms', name: 'micrograms', unit: 'micrograms', action: 'take', thing: 'tabs'},
    {key: 'u/ml', name: 'u/ml', unit: 'u/ml', action: 'inject', thing: 'units'},
    {key: 'mg/ml', name: 'mg/ml', unit: 'mg/ml', action: 'inject', thing: 'mg'},
    {key: 'mg caps', name: 'mg', unit: 'mg caps', action: 'inject', thing: 'caps'}
  ].freeze

  FREQUENCY_OPTIONS = [
    { key: 'daily', name: 'daily' },
    { key: 'every_other_day', name: 'every other day' },
    { key: '2_days_per_week', name: '2 days per week' },
    { key: '3_days_per_week', name: '3 days per week' },
    { key: '4_days_per_week', name: '4 days per week' },
    { key: '5_days_per_week', name: '5 days per week' },
    { key: '6_days_per_week', name: '6 days per week' },
    { key: 'weekly', name: 'weekly' },
    { key: 'twice_monthly', name: 'every two weeks' },
    { key: 'monthly', name: 'monthly' }
  ].freeze

  module ClassMethods

    # keep in sync with web version!
    def condition_text(treatment_type, data)
      if treatment_type == 'medication_tablets' || treatment_type == 'medication_injection'
        selected_unit = TreatmentBase::TREATMENT_UNITS.select{ |rec| rec[:key] == data["unit"] }[0]
        unit = selected_unit[:unit]
        action = selected_unit[:action]
        thing = selected_unit[:thing]

        schedule = []
        data["schedule"].each do |k,v|
          when_value = k == 'bedtime' ? 'at' : data["when"]
          schedule << "#{v} #{thing} #{when_value} #{k}" if v.to_f > 0
        end

        s = "#{data["dosage"]} #{unit}"
        s << (schedule.size > 0 ? ", #{action} #{schedule.join(', ')}" : "")
        s
      elsif treatment_type == 'exercise'
        "#{data['count']} #{data['condition']}, "
      end
    end

  end
end
