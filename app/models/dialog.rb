# encoding: utf-8

class Dialog

  def self.search(params = {})
    query = <<-SQL.strip_heredoc
      WITH
        "dialogs" AS (
          SELECT
            u.id AS user_id,
            m.id AS message_id,
            COUNT(um.id) AS unread_messages_count
          FROM users AS u
          LEFT JOIN messages AS m ON (
            m.is_last = TRUE
            AND ((m.sender_id = :self_id AND m.receiver_id = u.id) OR (m.sender_id = u.id AND m.receiver_id = :self_id))
          )
          LEFT JOIN messages AS um ON (
            um.is_read = FALSE
            AND (um.sender_id = u.id AND um.receiver_id = :self_id)
          )
          WHERE TRUE
            AND u.id <> :self_id
          GROUP BY
            u.id,
            m.id
          --ORDER BY
          --  m.created_at DESC NULLS LAST,
          --  u.full_name ASC
        )
      SELECT
        u.*,
        d.message_id,
        d.unread_messages_count
      FROM users AS u
      LEFT JOIN dialogs AS d ON u.id = d.user_id
      WHERE
        u.id <> :self_id
      ORDER BY
        d.message_id DESC NULLS LAST,
        u.full_name ASC
    SQL

    User.find_by_sql([query, self_id: params[:self_id]])
  end

  def self.dialogs_unread_count(self_id)
    query = <<-SQL.strip_heredoc
      SELECT COUNT(DISTINCT(sender_id))
      FROM messages
      WHERE TRUE
        AND receiver_id = :self_id
        AND is_read = FALSE
    SQL

    s = Message.count_by_sql([query, self_id: self_id])
    # _debug s
    s
  end

end
