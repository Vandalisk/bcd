# encoding: utf-8

require 'csv'
require 'fileutils'

module Sync
  class Export < ActiveRecord::Base
    self.table_name = 'sync_exports'

    serialize :options, JSON

    PER_STEP = 100.freeze

    after_create :update_total_count


    def update_total_count
      self.update_columns(total_count: self.build_query.count)
    end

    def what
      whats = [:tasks]
      @what ||= begin
        options["what"].to_sym if whats.map(&:to_s).include?(options["what"])
      end
    end

    def model_class
      case what
      when :tasks then Task
      end
    end

    def file
      File.join(Rails.root, "tmp", "sync", "exports", "#{what}_#{id}.csv").to_s
    end

    def build_query
      s = model_class.export_query(options)
      s = s.where("#{model_class.quoted_table_name}.updated_at >= ?", options["date_from"]) if options["date_from"]
      s = s.where("#{model_class.quoted_table_name}.updated_at <= ?", options["date_to"]) if options["date_to"]
      s = s.order(:id)
      s
    end

    def export_to_csv!
      # write BOM for stupid Excel, see gem "file_with_bom"
      if self.done_count == 0
        FileUtils.mkdir_p(File.dirname(self.file))
        File.open(self.file, "a+b") { |f| f << "\xEF\xBB\xBF" } # write BOM
        self.update_attributes(status: 'process')
      end

      s = build_query.limit(self.class::PER_STEP).where("#{model_class.quoted_table_name}.id > ?", self.last_id).all
      self.update_attributes(status: 'done') and return if s.count == 0

      CSV.open(self.file, "a+b", :col_sep => ',', :quote_char => '"', :row_sep => "\r\n") do |csv|
        self.transaction do

          done_count = self.done_count
          last_id = self.last_id
          items = model_class.prepare_export_items(s)

          csv << items[0].keys if self.done_count == 0

          items.each do |item|
            # last_id = item.delete(:id)
            csv << item.values
            done_count = done_count + 1
            last_id = item[:id]
          end

          self.update_attributes({
            done_count: done_count,
            last_id: last_id,
          })
        end
      end
    end

  end
end
