class WsNotifier
  def self.notify(faye_client, patient_id, sender, resource_type, event, data)
    opts = {
      resource_type: resource_type,
      event: event,
      data: data,
      sender: {
        id: sender[:id],
        name: sender[:full_name],
        avatar: sender[:gravatar_url].to_s+'&s=32'
      }
    }
    faye_client.publish "/notifications/#{patient_id}", opts
    second_receiver_id = resource_type == 'call' ? sender[:id] : 0
    faye_client.publish "/notifications/#{second_receiver_id}", opts
  end

  def self.notify_call(faye_client, receiver_id, sender, event, data)
    notify(faye_client, receiver_id, sender, 'call', event, data)
  end

  def self.notify_treatment(faye_client, receiver_id, sender, event, data)
    notify(faye_client, receiver_id, sender, 'treatment', event, data)
  end

  def self.notify_task(faye_client, receiver_id, sender, event, data)
    notify(faye_client, receiver_id, sender, 'task', event, data)
  end

  def self.notify_blood_glucose(faye_client, receiver_id, sender, event, data)
    notify(faye_client, receiver_id, sender, 'blood_glucose', event, data)
  end
end
