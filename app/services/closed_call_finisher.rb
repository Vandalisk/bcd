class ClosedCallFinisher
  def self.finish!
    TalkingStick::Room.where('last_used < ?', Time.now()-1.minute).delete_all
  end
end
