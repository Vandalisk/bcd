class OutdatedTreatmentsFinisher
  def self.finish!
    Treatment.outdated.each do |t|
      t.update(status: 'completed')
    end
  end
end
