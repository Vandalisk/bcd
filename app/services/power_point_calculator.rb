class PowerPointCalculator
  FREQUENCY_OPTIONS = {
   'daily':               1,
   'every_other_day':     2,
   '2_days_per_week':     4,
   '3_days_per_week':     2,
   '4_days_per_week':     2,
   '5_days_per_week':     1,
   '6_days_per_week':     1,
   'weekly':              8,
   'twice_monthly':       15,
   'monthly':             30
  }.freeze

  def self.calculate(power, frequency, quantity = 1)
    power * FREQUENCY_OPTIONS[frequency.to_sym] * quantity
  end
end
