class ComplianceCalculator
  def self.calculate!
    Treatment.all.each do |treatment|
      compliance_rate = calculate_compliance_for(treatment)
      treatment.update_attribute(:compliance_rate, compliance_rate)
    end

    User.patients.each do |patient|
      compliance_rate = calculate_compliance_for(patient)
      patient.update_attribute(:compliance_rate, compliance_rate)
    end
  end

  def self.calculate_compliance_for(target_object)
    target_object = target_object.treatment if target_object.is_a?(Task)

    condition =
      if target_object.is_a?(User)
        date = Date.today.last_week
        { date: date..date.end_of_week }
      elsif target_object.is_a?(Treatment)
        'date < now()'
      else
        ''
      end

    all_tasks = target_object.tasks.where(condition).count
    done_tasks = target_object.tasks.where(condition).where(is_completed: true).count

    done_tasks == 0 ? 0 : (done_tasks.to_f / all_tasks.to_f * 100).round
  end
end
