class UsersControllerPolicy < ApplicationPolicy
  attr_reader :user, :ctrlr

  def initialize(user, ctrlr)
    @user = user
    @ctrlr = ctrlr
  end

  def index?
    has_access?('private', user.role)
  end

  def create?
    has_access?('head', user.role)
  end

  %i(calculate_completed_tasks get_total_scores).each do |action|
    define_method("#{action}?") do
      has_access?('doctor', user.role) or user.id == params[:id]
    end
  end

  %i(show update).each do |action|
    define_method("#{action}?") do
      has_access?('doctor', user.role)
    end
  end

  #index используется только при поиске пользователей
  #create, судя по всему, может использоваться только докторами и админами для создания других докторов?
  #show могут только доктора, для сайдбара и диалогов
  #update может только доктор
  #delete не используется фронтом вообще
end
