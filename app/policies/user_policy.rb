class UserPolicy < ApplicationPolicy
  attr_reader :current_user, :user

  def initialize(current_user, user)
    @current_user = current_user
    @user = user
  end

  %i(calculate_completed_tasks get_total_scores).each do |action|
    define_method("#{action}?") do
      has_access?('doctor', current_user.role) or current_user == user
    end
  end
end
