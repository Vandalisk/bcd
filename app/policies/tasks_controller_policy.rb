class TasksControllerPolicy < ApplicationPolicy
  attr_reader :user, :ctrlr

  def initialize(user, ctrlr)
    @user = user
    @ctrlr = ctrlr
  end

  def index?
    has_access?('doctor', user.role)
  end

  def update?
    has_access?('doctor', user.role)
  end

  def submit_treatments?
    has_access?('doctor', user.role) || user.id == params[:receiver_id]
  end

  %i(update update_current destroy prescribe_requested_treatment create).each do |action|
    define_method("#{action}?") do
      has_access?('doctor', user.role)
    end
  end

  class Scope < TasksControllerPolicy
    attr_reader :user, :scope, :params
    def initialize(user, scope, params=false)
      @user = user
      @scope = scope
      @params = params
    end

    def resolve_list
      tasks = Task.all.joins('LEFT JOIN treatments ON treatments.id = tasks.treatment_id').includes(:treatment, :patient)
      tasks = tasks.select('tasks.*, treatments.name AS name, treatments.treatment_type AS treatment_type')

      if self.has_access?('doctor', user.role)
        tasks = tasks.where('treatments.receiver_id = ?', params[:treatment_receiver_id]) if params[:treatment_receiver_id]
      else
        tasks = tasks.where('treatments.receiver_id = ?', user.id)
      end
      tasks = tasks.where('treatments.status IN (?)', params[:treatment_status].split(',').map(&:strip)) if params[:treatment_status]
      tasks
    end

    def resolve_update
      raise 'not authorized' unless self.has_access?('doctor', user.role) || user.id == @scope.treatment.receiver_id
    end
  end
end
