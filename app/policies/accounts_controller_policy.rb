class AccountsControllerPolicy < ApplicationPolicy
  attr_reader :user, :ctrlr

  def initialize(user, ctrlr)
    @user = user
    @ctrlr = ctrlr
  end

  %i(patients staff).each do |action|
    define_method("#{action}?") do
      user.doctor? or user.case_manager? or user.admin?
    end
  end
end
