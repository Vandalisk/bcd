class TreatmentsControllerPolicy < ApplicationPolicy
  attr_reader :user, :ctrlr

  def initialize(user, ctrlr)
    @user = user
    @ctrlr = ctrlr
  end

  %i(update update_current destroy prescribe_requested_treatment create).each do |action|
    define_method("#{action}?") do
      has_access?('doctor', user.role)
    end
  end

  class Scope < TreatmentsControllerPolicy
    attr_reader :user, :scope, :receiver
    def initialize(user, scope, receiver = nil)
      @user = user
      @scope = scope
      @receiver = receiver
    end

    def resolve_list
      if self.has_access?('doctor', user.role)
        @scope.all
      else
        @scope.where(receiver_id: user.id)
      end
    end

    def resolve_templates
      if self.has_access?('doctor', user.role) || receiver == user
        scope.all
      else
        scope.none
      end
    end

    def resolve_update
      raise 'not authorized' unless self.has_access?('doctor', user.role) || user.id == @scope.receiver_id
    end
  end
end
