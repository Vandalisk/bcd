class MessagePolicy < ApplicationPolicy
  attr_reader :user, :message

  def initialize(user, message)
    @user = user
    @message = message
  end

  def show?
    self.has_access?(:head, user.role) || user == 
      message.sender || user == @message.receiver
  end

end
