class TreatmentTemplatesControllerPolicy < ApplicationPolicy
  attr_reader :user, :ctrlr

  def initialize(user, ctrlr)
    @user = user
    @ctrlr = ctrlr
  end

  def index?
    has_access?('private', user.role)
  end

  %i(show create update destroy).each do |action|
    define_method("#{action}?") do
      has_access?('head', user.role)
    end
  end
end
