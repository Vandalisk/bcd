class BloodGlucosePolicy < ApplicationPolicy
  attr_reader :user, :scope

  def initialize(user, scope)
    @user = user
    @scope = scope
  end

  %i(index create update destroy).each do |action|
    define_method("#{action}?") do
      user.admin? or user.doctor? or user.case_manager? or (user.patient? && user == scope.patient) if user
    end
  end
end
