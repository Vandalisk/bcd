# encoding: utf-8
class UserMailer < ActionMailer::Base
  default content_type: "text/html", parts_order: ["text/html", "text/enriched", "text/plain"]

  def invitation(options)
    @user = options[:user]
    @token = options[:token]
    mail({
      to: @user.email,
      subject: I18n.t('user_mailer.subject.invitation'),
      from: ENV["ADMIN_EMAIL"],
    })
  end

  def reset_password(options)
    @user = options[:user]
    @token = options[:token]
    mail({
      to: @user.email,
      subject: I18n.t('user_mailer.subject.password_reset_link'),
      from: ENV["ADMIN_EMAIL"],
    })
  end
end
