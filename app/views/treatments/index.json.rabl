collection :@treatments, root: :collection

attributes :id, :sender_id, :receiver_id, :treatment_template_id, :treatment_type, :name, :data, :frequency, :period,
  :date_start, :date_end, :description, :status, :completion_percent, :full_name,
  :treatment_template_full_name, :compliance_rate, :doctor_comment, :patient_comment, :scores,
  :accepted_by_full_name, :declined_by_full_name, :doctor_full_name, :patient_full_name, :sender_gravatar_url, :receiver_gravatar_url,:suggest_or_request, :dpd_dosage, :impact, :power, :insurance

node(:created_at) { |treatment| treatment.created_at.to_datetime }
node(:updated_at) { |treatment| treatment.updated_at.to_datetime }
node(:declined_at) { |treatment| treatment.declined_at.try(:to_datetime) }
node(:score_per_task) { |treatment| treatment.tasks.first.points if treatment.tasks.any? }
