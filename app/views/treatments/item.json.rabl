object :@treatment => :item

attributes :id, :sender_id, :receiver_id, :treatment_template_id, :treatment_type, :name, :data, :frequency, :period,
  :date_start, :date_end, :description, :status, :completion_percent, :full_name,
  :suggest_or_request, :doctor_full_name, :patient_full_name, :treatment_template_full_name, :accepted_by_full_name, :declined_by_full_name, :compliance_rate, :doctor_comment, :patient_comment, :sender_gravatar_url, :receiver_gravatar_url, :impact, :power, :insurance,
  :scores

node(:created_at) { |treatment| treatment.created_at.to_datetime }
node(:updated_at) { |treatment| treatment.updated_at.to_datetime }
node(:declined_at) { |treatment| treatment.declined_at.try(:to_datetime) }
