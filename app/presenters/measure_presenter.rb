class MeasurePresenter
  def initialize(measure)
    @measure = measure
  end

  def create_date(params)
    %i(year month).map { |time| params[time] ||= Date.today.public_send(time) }
    Date.new(params[:year].to_i, params[:month].to_i, 1)
  end
end
