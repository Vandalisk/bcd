set :stage, :production

role :app, %w(deploy@95.213.200.140)
role :web, %w(deploy@95.213.200.140)
role :db, %w(deploy@95.213.200.140)

server '95.213.200.140', user: 'deploy', roles: %w(web app)

set :deploy_to, '/var/local/apps/bcd-backend'
set :unicorn_rack_env, 'production'
set :rails_env, 'production'
set :ssh_options, forward_agent: true

set :rollbar_token, 'd4f4e8b4ec8e41a8a29a841c2ee3a674'
set :rollbar_env, Proc.new { fetch :stage }
set :rollbar_role, Proc.new { :app }
