# encoding: utf-8

Rails.application.routes.draw do
  mount TalkingStick::Engine, at: '/talking_stick'
  post '/talking_stick/rooms/:room_id/ice_connected', to: 'talking_stick/participants#ice_connected'

  post "auth/login",                    to: "auth#login"
  post "auth/logout",                   to: "auth#logout"
  get  "auth/profile",                  to: "auth#profile"
  put  "auth/profile",                  to: "auth#profile_submit"
  get  "auth/token/:token_type/:token", to: "auth#find_by_token"
  post "auth/send_reset_password_link", to: "auth#send_reset_password_link"
  post "auth/update_password",          to: "auth#update_password"

  resources :users, except: [:new, :edit, :destroy] do
    get :calculate_completed_tasks, on: :member
    get :get_total_scores, on: :member
    get :check_phn, on: :member
  end

  resources :treatment_templates, except: [:new, :edit] do
    get :check_by_name, on: :collection
  end

  resources :treatments, except: [:new, :edit] do
    post :submit_treatments, on: :collection
    put :prescribe_requested_treatment, on: :member
    put :update_current, on: :member
  end

  resources :tasks, except: [:new, :create, :edit, :show, :destroy] do
    put :update_all, on: :collection
  end

  resource :accounts, only: [:patients, :staff] do
    get :patients
    get :staff
  end

  namespace :sync do
    resources :exports, only: [:index, :create] do
      post :to_csv, on: :member
      get :download, on: :member
    end
    # resources :imports, :only => [:index, :create] do
    #   get :rollback, :on => :collection
    # end
  end

  get '/lab_results/from_dpd', to: 'lab_results#get_from_dpd'

  resources :dialogs, only: [:index] do
    get :dialogs_unread_count, on: :collection
  end
  resources :messages, only: [:index, :create] do
    collection do
      put :read_multiple
    end
  end

  resources :blood_glucoses, except: [:show]
end
