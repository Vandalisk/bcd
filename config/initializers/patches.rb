# encoding: utf-8
module Mail
  class FileDelivery
    attr_accessor :settings
    def initialize(values = {})
      @settings = { :location => './mails' }.merge!(values)
    end

    def deliver!(mail)
      if ::File.respond_to?(:makedirs)
        ::File.makedirs settings[:location]
      else
        ::FileUtils.mkdir_p settings[:location]
      end

      mail.destinations.uniq.each_with_index do |to, i|
        time_prefix = "#{Time.now.to_i}_#{Time.now.usec}"
        file = "#{time_prefix}_#{i}_#{File.basename(to.to_s)}.eml"
        ::File.open(::File.join(settings[:location], file), 'a') { |f| "#{f.write(mail.encoded)}\r\n\r\n" }
      end
    end
  end
end
