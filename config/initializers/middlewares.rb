# encoding: utf-8

require 'faye'
Faye::WebSocket.load_adapter('thin')

Rails.application.config.middleware.insert_before 0, Rack::Cors, debug: true do
  allow do
    # origins ENV["ALLOW_ORIGINS"].split("|")
    origins '*'
    resource '*', headers: :any, methods: [:get, :post, :put, :patch, :delete, :options], max_age: 0
  end
end

Rails.application.config.middleware.delete Rack::Lock
Rails.application.config.middleware.use Faye::RackAdapter, {
  mount: ENV['FAYE_MOUNT'],
  timeout: ENV['FAYE_TIMEOUT'].to_i,
  ping: ENV['FAYE_TIMEOUT'].to_i,
  extensions: [
    FayeExt::Auth.new,
    FayeExt::Messages.new,
    FayeExt::ClearExt.new,
  ],
} do |fs| end
