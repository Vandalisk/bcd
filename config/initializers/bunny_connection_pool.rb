$bunny_connection = ConnectionPool.new { BunnyFactory.build }
ObjectSpace.define_finalizer(
  $bunny_connection, proc do
    $bunny_connection.shutdown { |connection| connection.close }
    Rails.logger.info "RabbitMQ connection closed"
  end
)
