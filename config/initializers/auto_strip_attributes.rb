
AutoStripAttributes::Config.setup do
  filters_enabled[:strip] = true
  filters_enabled[:nullify] = true
  filters_enabled[:squish] = true
end
