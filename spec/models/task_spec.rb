require "rails_helper"

RSpec.describe Task, type: :model do
  describe "Task" do
    before(:all){
      Task.skip_callback(:update, :after, :update_treatment_completion)
      Treatment.skip_callback(:save, :after, :set_full_name)
      Treatment.skip_callback(:save, :after, :save_event_to_feed)
    }
    describe "after update should calculate" do
      let(:treatment) { create(:treatment) }

      context "task change is_completed to true" do
        # is_completed from false to true
        let(:task) { treatment.tasks.first }
        let!(:points) { task.points }
        let!(:scores) { treatment.scores }
        let!(:current_score) { treatment.patient.current_score }

        it "should increase points to user's current score and treatment's scores" do
          task.update_attributes(is_completed: true)
          expect(treatment.patient.reload.current_score).to eq(current_score + points)
        end
      end

      context "task change is_completed to false" do
        let!(:task) do
          t = treatment.tasks.first
          t.complete!
          t
        end
        let!(:points) { task.points }
        let!(:scores) { treatment.scores }
        let!(:current_score) { treatment.patient.reload.current_score }
        # is_completed from true to false
        it "should decrease points to user's current score and treatment's scores" do
          task.update_attributes(is_completed: false)
          expect(treatment.patient.reload.current_score).to eq(current_score - points)
        end
      end
    end
  end
end
