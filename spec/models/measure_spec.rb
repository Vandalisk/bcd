require 'rails_helper'

RSpec.describe Measure, type: :model do
  describe '.search' do
    let(:patient) { create :patient }
    let :measures do
      3.times do |i|
        create :measure, patient: patient, date: Date.today, value: i % 2
      end
    end
    let :params do
      {
        patient_id: patient.id,
        value: 1
      }
    end

    it 'returns right measures' do
      ids = Measure.where(value: 1).pluck(:id)
      expect(Measure.search(params).map(&:id)).to eq ids
    end
  end
end
