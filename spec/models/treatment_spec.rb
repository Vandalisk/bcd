require "rails_helper"

RSpec.describe Treatment, type: :model do
  describe "after update" do
    let!(:treatment) { create(:treatment) }
    let(:patient) { treatment.patient }

    describe "check scores calculator" do
      before(:all){
        Treatment.skip_callback(:save, :after, :to_current_callback)
        Treatment.skip_callback(:save, :after, :set_full_name)
        Treatment.skip_callback(:save, :after, :save_event_to_feed)
      }
      describe "treatment status changed to active" do
        let!(:treatment) { create(:treatment, status: 'new') }

        it "should increase patient's total_score with treatment's points count" do
          old_score = patient.total_score
          treatment.update_columns(status: 'active')
          expect(patient.reload.total_score).to eq(old_score + treatment.scores)
        end
      end

      describe "treatment status changed from active to declined" do
        it "should decrease patient's total_score with treatment's points count" do
          old_score = patient.total_score
          treatment.update_columns(status: 'stopped')
          expect(patient.reload.total_score).to eq(old_score - treatment.scores)
        end
      end

      describe "treatment status changed from active to stopped" do
        it "should decrease patient's total_score with treatment's points count" do
          old_score = patient.total_score
          treatment.update_columns(status: 'declined')
          expect(patient.reload.total_score).to eq(old_score - treatment.scores)
        end
      end

      describe "treatment status changed from active to stopped" do
        it "should decrease patient's total_score with treatment's points count" do
          old_score = patient.total_score
          treatment.update_columns(status: 'new')
          expect(patient.reload.total_score).to eq(old_score - treatment.scores)
        end
      end
    end
  end
end
