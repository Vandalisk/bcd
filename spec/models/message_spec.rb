require 'rails_helper'

RSpec.describe Message, type: :model do
  describe 'prepare for rabbit' do
    let(:patients) { create_list(:patient, 2) }
    let(:doctor) { create(:doctor) }
    let!(:messages) do
      create_list(:message, 5, sender: doctor, receiver: patients.first) +
      create_list(:message, 5, sender: patients.first, receiver: doctor) +
      create_list(:message, 5, sender: doctor, receiver: patients.second) +
      create_list(:message, 5, sender: patients.second, receiver: doctor)
    end

    context 'valid params' do
      let(:valid_params) do
        {
          "patient_ids" => patients.map(&:id),
          "doctor_id" => doctor.id,
          "date_from" => (Date.today - 1).strftime,
          "date_to" => Date.today.strftime
        }
      end

      it 'corresponding result' do
        expect(subject.class.prepare_for_rabbit(valid_params).length).to eq messages.count
      end

      it 'fields must be the same' do
        results = subject.class.prepare_for_rabbit(valid_params)

        results.order(:id).each_with_index do |result, index|
          ["id", "sender_id", "receiver_id", "text"].each do |key|
            expect(result.send(key)).to eq(messages[index].send(key))
          end
        end
      end
    end

    context 'invalid params' do
      let(:invalid_params){ { "doctor_id" => nil } }

      it 'result should be 0' do
        expect(subject.class.prepare_for_rabbit(invalid_params).length).to eq 0
      end
    end
  end

  describe '#set_last' do
    let(:patient) { create :patient }
    let(:doctor) { create :doctor }
    let!(:messages) { create_list :message, 5, sender: patient, receiver: doctor }

    it 'marks new messages as last' do
      message = Message.last

      expect do
        create(:message, sender: message.sender, receiver: message.receiver)
        message.reload
      end.to change(message, :is_last)
    end
  end

  describe '#prepare_for_json' do
    it 'adds new fields for json' do
      expect(subject.prepare_for_json.include?("sender_gravatar_url")).to be_truthy
      expect(subject.prepare_for_json.include?("sender_full_name")).to be_truthy
    end
  end
end
