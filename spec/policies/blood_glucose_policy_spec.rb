require 'rails_helper'

describe BloodGlucosePolicy do
  subject { BloodGlucosePolicy }
  let(:measure) { create :measure }
  let(:doctor) { create :doctor }
  let(:case_manager) { create :case_manager }
  let(:patient) { create :patient }

  permissions :update?, :create?, :destroy?, :index? do
    describe "guest" do

      it "denies access" do
        expect(subject).not_to permit(nil, measure)
      end
    end

    describe "doctor" do
      it "allows access" do
        expect(subject).to permit(doctor, measure)
      end
    end

    describe "case_manager" do
      it "allows access" do
        expect(subject).to permit(case_manager, measure)
      end
    end

    describe "patient" do
      context "his" do
        let(:measure) { create :measure, patient_id: patient.id }

        it "allows" do
          expect(subject).to permit(patient, measure)
        end
      end
      context "another" do
        it "denies" do
          expect(subject).not_to permit(patient, measure)
        end
      end
    end
  end
end
