require "rails_helper"

RSpec.describe TreatmentTemplatesController, type: :controller do
  let(:headers) { { "CONTENT_TYPE" => "application/json" } }
  let(:doctor) { create(:doctor) }
  let!(:treatment_template) { create :treatment_template,
                              treatment_type: 'medical_injection' }
  describe 'GET index' do
    let :params do
      {}
    end

    context 'without sign in' do
      it 'tries to get templates with params' do
        get :index, params

        expect(response.status).to eq 401
      end
    end

    context 'with sign in' do
      before(:each) { login doctor }

      it 'tries to get templates with params' do
        get :index, params, format: :json
        ids = json['collection'].map { |template| template['id'] }
        expect(ids).to eq(TreatmentTemplate.all.pluck(:id))
      end
    end
  end

  describe 'POST create' do
    let :valid_params do
      {
        author_id: doctor.id,
        treatment_type: 'medical_injection',
        name: 'a painful injection',
        frequency: 'daily',
        insurance: 'a solid one',
        power: 5
      }
    end
    let(:invalid_params) { valid_params.merge(name: "") }

    context 'without sign in' do
      it 'tries to create the template with invalid params' do
        get :create, invalid_params
        expect(response.status).to eq 401
      end

      it 'tries to create the template with valid params' do
        get :create, invalid_params
        expect(response.status).to eq 401
      end
    end

    context 'with sign in' do
      before(:each) { login doctor }

      context 'with invalid params' do
        it 'tries to create template' do
          get :create, invalid_params
          expect(response.status).to eq 422
        end

        it "doesn't create the template" do
          expect {
            get :create, invalid_params
          }.not_to change(TreatmentTemplate, :count)
        end
      end

      context 'with valid params' do
        it 'tries to create template' do
          get :create, valid_params
          expect(response.status).to eq 201
        end

        it 'creates the template' do
          expect {
            get :create, valid_params
          }.to change(TreatmentTemplate, :count).by(1)
        end
      end
    end
  end

  describe 'PUT update' do
    let :valid_params do
      {
        id: treatment_template.id,
        name: 'a painful injection'
      }
    end
    let(:invalid_params) { valid_params.merge(name: "") }
    context 'without sign in' do
      it 'tries to update template with invalid params' do
        put :update, invalid_params
        expect(response.status).to eq 401
      end

      it 'tries to update tepmlate with valid params' do
        put :update, valid_params
        expect(response.status).to eq 401
      end
    end

    context 'with sign in' do
      before(:each) { login doctor }

      context 'with invalid params' do
        it 'tries to update the template' do
          put :update, invalid_params
          expect(response.status).to eq 422
        end

        it "doesn't update the template" do
          expect {
            put :update, invalid_params
            treatment_template.reload
          }.not_to change(treatment_template, :name)
        end
      end

      context 'with valid params' do
        it 'tries to update template' do
          put :update, valid_params
          expect(response.status).to eq 200
        end

        it 'updates the template' do
          expect {
            put :update, valid_params
            treatment_template.reload
          }.to change(treatment_template, :name)
        end
      end
    end
  end

  describe 'GET show' do
    let(:params) { { id: treatment_template.id } }

    it 'tries to get the template without sign in' do
      get :show, params
      expect(response.status).to eq 401
    end

    it 'tries to get the temlpate with sign in' do
      login doctor
      get :show, params
      expect(json['item']['id']).to eq treatment_template.id
    end
  end

  describe 'DELETE destroy' do
    let(:params) { { id: treatment_template.id } }

    context 'without sign in' do
      it 'tries to delete the template with invalid params' do
        delete :destroy, params
        expect(response.status).to eq 401
      end
    end

    context 'with sign in' do
      before(:each) { login doctor }

      it 'tries to delete the template' do
        delete :destroy, params

        expect(response.status).to eq 200
      end

      it 'deletes the template' do
        expect {
          delete :destroy, params
        }.to change(TreatmentTemplate, :count).by(-1)
      end
    end
  end

  describe 'GET check_by_name' do
    let :valid_params do
      {
        id: treatment_template.id,
        name: treatment_template.name
      }
    end
    let(:second_template) { create :treatment_template, name: 'second one' }
    let(:invalid_params) { valid_params.merge(name: second_template.name) }

    context 'without sign in' do
      it 'checks if the name and id belong to the same object with invalid params' do
        get :check_by_name, invalid_params
        expect(response.status).to eql 401
      end

      it 'checks if the name and id belong to the same object with valid params' do
        get :check_by_name, valid_params
        expect(response.status).to eql 401
      end
    end

    context 'with sign in' do
      before(:each) { login doctor }

      it 'checks if the name and id belong to the same object with invalid params' do
        get :check_by_name, invalid_params
        expect(json['item']).to be_truthy
      end

      it 'checks if the name and id belong to the same object with valid params' do
        get :check_by_name, valid_params
        expect(json['item']).to be_falsy
      end
    end
  end
end
