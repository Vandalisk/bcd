require "rails_helper"
RSpec.describe BloodGlucosesController, type: :controller do
  let(:patient) { create :patient }
  let(:doctor) { create(:doctor) }
  let(:case_manager) { create(:case_manager) }
  let!(:measure) { create :measure }
  let!(:patients_measure) { create :measure, patient_id: patient.id }
  let(:patients_measure_response) do
    {
      "date"=> patients_measure.date.strftime,
      "measurements"=>[
        { "id"=> patients_measure.id, "value"=> patients_measure.value.to_f.to_s, "when"=> patients_measure.when }
      ]
    }
  end

  describe "GET index" do
    let!(:blood_glucoses) { JSON.parse(File.read("spec/fixtures/blood_glucoses.json")) }
    let!(:measurements) do
      blood_glucoses.each do |objects|
        objects["measurements"].each do |measurement|
          measurement["date"] = objects["date"]
          Measure.create(measurement)
          measurement.delete("date")
          measurement.delete("patient_id")
        end
      end
    end

    context 'without sign in' do
      it 'tries to get blood glucoses with valid params' do
        get :index, month: 12, year: 2015
        expect(response.status).to eq 401
      end
    end

    context 'signed in as doctor or case_manager' do
      context 'doctor' do
        before(:each) { login doctor }

        it 'tries to get blood glucoses with params' do
          get :index, month: 12, year: 2015

          expect(response.status).to eq 200
          expect(json.length).to eq(blood_glucoses.length)
          expect(json).to eq(blood_glucoses)
        end
      end

      context 'case_manager' do
        before(:each) { login case_manager }

        it 'tries to get blood glucoses with params' do
          get :index, month: 12, year: 2015

          expect(response.status).to eq 200
          expect(json.length).to eq(blood_glucoses.length)
          expect(json).to eq(blood_glucoses)
        end
      end
    end

    context 'signed in as patient' do
      before(:each) { login patient }

      it 'tries to get blood glucoses with params' do
        get :index, month: 12, year: 2015

        expect(response.status).to eq 403
        expect(json["message"]).to eq "Access Forbidden"
      end

      it "tries to get blood glucoses with patient_id that equals with patient's id in params" do
        get :index, month: patients_measure.date.month, year: patients_measure.date.year, patient_id: patient.id

        expect(response.status).to eq 200
        expect(json.length).to eq(1)
        expect(json).to eq [patients_measure_response]
      end

      it 'tries to get blood glucoses with wrong patient_id in params' do
        get :index, month: 12, year: 2015, patient_id: 2

        expect(response.status).to eq 403
        expect(json["message"]).to eq "Access Forbidden"
      end
    end
  end

  describe "POST create" do
    let(:valid_params) { {"date": "2015-08-12", "when": "before_breakfast", "value": 14.2 } }
    let(:invalid_params) { valid_params.merge "date": nil }
    let(:valid_params_for_patient) { valid_params.merge "patient_id": patient.id }
    let(:invalid_params_for_patient) { valid_params_for_patient.merge "date": nil }

    context 'without sign in' do
      it 'tries to create blood glucose with valid params' do
        get :create, valid_params
        expect(response.status).to eq 401
      end

      it 'tries to create blood glucose with invalid params' do
        get :create, "date": nil
        expect(response.status).to eq 401
      end
    end

    context 'signed in as doctor or case_manager' do
      context 'doctor' do
        before(:each) { login doctor }

        it 'tries to create blood glucose with valid params' do
          get :create, valid_params

          expect(response.status).to eq 201
        end

        it 'tries to create blood glucose with invalid params' do
          get :create, invalid_params

          expect(response.status).to eq 422
          expect(json["message"]).to eq "Validation Failed"
        end
      end

      context 'case_manager' do
        before(:each) { login case_manager }

        it 'tries to create blood glucose with valid params' do
          get :create, valid_params

          expect(response.status).to eq 201
        end

        it 'tries to create blood glucose with invalid params' do
          get :create, invalid_params

          expect(response.status).to eq 422
          expect(json["message"]).to eq "Validation Failed"
        end
      end
    end

    context 'signed in as patient' do
      before(:each) { login patient }

      context 'with usual params' do
        it 'tries to create blood glucose with valid params' do
          get :create, valid_params

          expect(response.status).to eq 403
          expect(json["message"]).to eq "Access Forbidden"
        end

        it 'tries to create blood glucose with invalid params' do
          get :create, invalid_params

          expect(response.status).to eq 403
          expect(json["message"]).to eq "Access Forbidden"
        end
      end

      context 'with params for patient' do
        it 'tries to create blood glucose with valid params' do
          get :create, valid_params_for_patient

          expect(response.status).to eq 201
        end

        it 'tries to create blood glucose with invalid params' do
          get :create, invalid_params_for_patient

          expect(response.status).to eq 422
          expect(json["message"]).to eq "Validation Failed"
        end
      end
    end
  end

  describe 'PUT #update' do
    let(:valid_params) { { id: measure.to_param, "date": "2015-08-12", "when": "before_breakfast", "value": 14.2 } }
    let(:invalid_params) { valid_params.merge "date": nil }

    context 'without sign in' do
      it 'tries to update blood glucose with valid params' do
        put :update, valid_params
        expect(response.status).to eq 401
      end

      it 'tries to update blood glucose with invalid params' do
        put :update, invalid_params
        expect(response.status).to eq 401
      end
    end

    context 'signed in as doctor or case_manager' do
      context 'doctor' do
        before(:each) { login doctor }

        it 'tries to update blood glucose with valid params' do
          put :update, valid_params

          expect(response.status).to eq 201
        end

        it 'tries to update blood glucose with invalid params' do
          put :update, invalid_params

          expect(response.status).to eq 422
          expect(json["message"]).to eq "Validation Failed"
        end
      end

      context 'case_manager' do
        before(:each) { login case_manager }

        it 'tries to update blood glucose with valid params' do
          put :update, valid_params

          expect(response.status).to eq 201
        end

        it 'tries to update blood glucose with invalid params' do
          put :update, invalid_params

          expect(response.status).to eq 422
          expect(json["message"]).to eq "Validation Failed"
        end
      end
    end

    context 'signed in as patient' do
      before(:each) { login patient }

      context 'with usual params' do
        it 'tries to update blood glucose with valid params' do
          put :update, valid_params

          expect(response.status).to eq 403
          expect(json["message"]).to eq "Access Forbidden"
        end

        it 'tries to update blood glucose with invalid params' do
          put :update, invalid_params

          expect(response.status).to eq 403
          expect(json["message"]).to eq "Access Forbidden"
        end
      end

      context 'with params for patient' do
        context "tries to update any blood glucose" do
          it 'tries to update blood glucose with valid params' do
            put :update, valid_params

            expect(response.status).to eq 403
            expect(json["message"]).to eq "Access Forbidden"
          end

          it 'tries to update blood glucose with invalid params' do
            put :update, invalid_params

            expect(response.status).to eq 403
            expect(json["message"]).to eq "Access Forbidden"
          end
        end

        context "tries to update patient's blood glucose" do
          let(:valid_params) { { id: patients_measure.to_param, "date": "2015-08-12", "when": "before_breakfast", "value": 14.2 } }
          let(:valid_params_for_patient) { valid_params.merge "patient_id": patient.id }
          let(:invalid_params_for_patient) { valid_params_for_patient.merge "date": nil }

          it 'tries to update blood glucose with valid params' do
            put :update, valid_params_for_patient

            expect(response.status).to eq 201
          end

          it 'tries to update blood glucose with invalid params' do
            put :update, invalid_params_for_patient

            expect(response.status).to eq 422
            expect(json["message"]).to eq "Validation Failed"
          end
        end
      end
    end
  end

  describe "DELETE #destroy" do
    context 'without sign in' do
      it 'tries to delete blood glucose with valid params' do
        delete :destroy, { id: measure.id }
        expect(response.status).to eq 401
      end
    end

    context 'signed in as doctor or case_manager' do
      context 'doctor' do
        before(:each) { login doctor }

        it 'tries to delete blood glucose with valid params' do
          put :destroy, { id: measure.id }

          expect(response.status).to eq 200
        end

        it 'destroys the requested measure' do
          expect {
            delete :destroy, { id: measure.to_param }
          }.to change(Measure, :count).by(-1)
        end
      end

      context 'case_manager' do
        before(:each) { login case_manager }

        it 'tries to delete blood glucose with valid params' do
          put :destroy, { id: measure.id }

          expect(response.status).to eq 200
        end

        it 'destroys the requested measure' do
          expect {
            delete :destroy, { id: measure.to_param }
          }.to change(Measure, :count).by(-1)
        end
      end
    end

    context 'signed in as patient' do
      before(:each) { login patient }

      context 'with usual params' do
        it 'tries to delete blood glucose with valid params' do
          put :destroy, { id: measure.id }

          expect(response.status).to eq 403
          expect(json["message"]).to eq "Access Forbidden"
        end
      end

      context 'with params for patient' do
        context "tries to delete any blood glucose" do
          it 'tries to delete blood glucose with valid params' do
            put :destroy, { id: measure.id }

            expect(response.status).to eq 403
            expect(json["message"]).to eq "Access Forbidden"
          end
        end

        context "tries to delete patient's blood glucose" do
          it 'tries to delete blood glucose with valid params' do
            put :destroy, { id: patients_measure.id }

            expect(response.status).to eq 200
          end
        end
      end
    end
  end
end
