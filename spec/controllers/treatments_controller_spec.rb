require "rails_helper"

RSpec.describe TreatmentsController, type: :controller do

  let(:headers) { { "CONTENT_TYPE" => "application/json" } }
  let(:doctor) { create(:doctor) }
  let!(:treatment) { create(:treatment, treatment_type: 'medication_injection') }

  describe 'PUT update' do
    describe 'ToDPD::Notifier check' do
      before(:each) { login doctor }
      context 'dpd.treatment.prescribe' do
        context 'valid_params' do
          let!(:treatment) do
            create(:treatment,
              treatment_type: 'medication_injection',
              status: (Treatment::STATUSES - [:from_dpd, :active]).sample
            )
          end
          let(:routing_key) { 'dpd.treatment.prescribe' }
          let(:valid_params) do
            {
              id: treatment.id,
              status: 'active'
            }
          end

          it 'routing_key correct' do
            feed_event = double(:feed_event)
            allow_any_instance_of(Treatment).to receive_message_chain(:feed_events, :last).and_return(feed_event)
            allow(feed_event).to receive(:update)
            allow(feed_event).to receive(:where)
            expect(ToDPD::Notifier).to receive(:new).with(routing_key)
            put :update, valid_params, headers
          end

          it 'notify performs' do
            notifier = double(:notifier)
            feed_event = double(:feed_event)
            allow(ToDPD::Notifier).to receive(:new).and_return(notifier)
            allow_any_instance_of(Treatment).to receive_message_chain(:feed_events, :last).and_return(feed_event)
            allow(feed_event).to receive(:update)
            allow(feed_event).to receive(:where)
            expect(notifier).to receive(:notify)
            put :update, valid_params, headers
          end
        end
      end
      context 'dpd.treatment.update' do
        let(:routing_key) { 'dpd.treatment.update' }
        let(:valid_params) do
          {
            id: treatment.id,
            description: FFaker::Lorem.paragraph
          }
        end

        it 'performs' do
          expect(ToDPD::Notifier).to receive(:new).with(routing_key)
          put :update, valid_params, headers
        end

        it 'notify performs' do
          notifier = double(:notifier)
          allow(ToDPD::Notifier).to receive(:new).and_return(notifier)
          expect(notifier).to receive(:notify)
          put :update, valid_params, headers
        end
      end
      context 'dpd.treatment.delete' do
        let(:routing_key) { 'dpd.treatment.delete' }
        let(:valid_params) do
          {
            id: treatment.id,
            status: (Treatment::STATUSES - [:from_dpd, :active]).sample
          }
        end

        it 'performs' do
          feed_event = double(:feed_event)
          allow_any_instance_of(Treatment).to receive_message_chain(:feed_events, :last).and_return(feed_event)
          allow(feed_event).to receive(:update)
          expect(ToDPD::Notifier).to receive(:new).with(routing_key)
          put :update, valid_params, headers
        end

        it 'notify performs' do
          notifier = double(:notifier)
          feed_event = double(:feed_event)
          allow(ToDPD::Notifier).to receive(:new).and_return(notifier)
          allow_any_instance_of(Treatment).to receive_message_chain(:feed_events, :last).and_return(feed_event)
          allow(feed_event).to receive(:update)
          expect(notifier).to receive(:notify)
          put :update, valid_params, headers
        end
      end
    end

    describe 'Database update' do
      let!(:treatment) do
        create(:treatment,
               treatment_type: 'excercise',
               status: 'new'
              )
      end

      let(:valid_params) do
        {
          id: treatment.id,
          status: 'active'
        }
      end

      let(:invalid_params) do
        {
          id: treatment.id,
          status: 'invalid status'
        }
      end

      context 'without sign in' do
        it 'tries to update the treatment with valid params' do
          put :update, valid_params, headers
          expect(response.status).to eq 401
        end

        it 'tries to update the treatment with invalid params' do
          put :update, invalid_params, headers
          expect(response.status).to eq 401
        end
      end

      context 'signed in' do
        context 'as a doctor' do
          before(:each) { login doctor }

          context 'with valid params' do

            it 'tries to update the treatment ' do
              put :update, valid_params, headers

              expect(response.status).to eq 200
            end

            it 'updates the treatment' do
              expect {
                put :update, valid_params, headers
                treatment.reload
              }.to change(treatment, :status)
            end
          end

          context 'with invalid params' do

            it 'tries to update the treatment with invalid params' do
              put :update, invalid_params, headers

              expect(response.status).to eq 422
            end

            it "doesn't update the treatment" do
              expect {
                put :update, invalid_params, headers
                treatment.reload
              }.not_to change(treatment, :status)
            end
          end
        end

        context 'as a receiver' do
          before(:each) do
            login treatment.patient
          end

          context 'with valid params' do
            it 'tries to update the treatment' do
              put :update, valid_params, headers

              expect(response.status).to eq 200
            end

            it 'updates the treatment' do
              expect {
                put :update, valid_params
                treatment.reload
              }.to change(treatment, :status)
            end
          end

          context 'with invalid params' do
            it 'tries to update the treatment' do
              put :update, invalid_params, headers

              expect(response.status).to eq 422
            end

            it "doesn't update the treatment" do
              expect {
                put :update, invalid_params, headers
                treatment.reload
              }.not_to change(treatment, :status)
            end
          end
        end
      end
    end
  end

  describe 'GET index' do
    render_views
    let(:params) { { format: :json } }

    context 'without sign in' do
      it 'tries to get treatments with valid params' do
        get :index, params
        expect(response.status).to eq 401
      end
    end

    context 'with sign in' do
      context 'as a doctor' do
        before(:each) { login doctor }

        it 'tries to get treatments with params' do
          get :index, params, format: :json

          ids = json['collection'].map { |template| template['id'] }
          expect(ids).to eq(Treatment.all.pluck(:id))
        end
      end

      context 'as a receiver' do
        before(:each) { login treatment.patient }

        it 'tries to get treatments with params' do
          get :index, params

          ids = json['collection'].map { |template| template['id'] }
          expect(ids).to eq(TreatmentTemplate.all.pluck(:id))
        end
      end
    end
  end

  describe 'POST create' do
    let(:patient) { create :patient }
    let(:template) { create :treatment_template }
    let :valid_params do
      {
        name: 'Valid params name',
        status: 'active',
        receiver_id: patient.id,
        treatment_template_id: template.id
      }
    end
    let(:invalid_params) { valid_params.merge(name: "") }

    context 'without sign in' do
      it 'tries to create a treatment with valid params' do
        get :create, valid_params

        expect(response.status).to eq 401
      end

      it 'tries to create a treatment with invalid params' do
        get :create, invalid_params

        expect(response.status).to eq 401
      end
    end

    context 'signed in as a doctor' do
      before(:each) { login doctor }

      it "tries to create the treatment with invalid params" do
        get :create, invalid_params

        expect(response.status).to eq 422
      end

      context 'with valid params' do
        it 'tries to add the treatment' do
          get :create, valid_params
          expect(response.status).to eq 200
        end

        it 'creates the treatment' do
          expect {
            get :create, valid_params
          }.to change(Treatment, :count).by(1)
        end
      end
    end
  end

  describe 'DELETE #destroy' do
    let(:patient) { treatment.patient }

    context 'without sign in' do
      it 'tries to delete a treatment with valid params' do
        delete :destroy, { id: treatment.id }
        expect(response.status).to eq 401
      end
    end

    context 'signed in' do
      context 'as a doctor' do
        before(:each) { login doctor }

        it 'tries to delete a treatment' do
          delete :destroy, { id: treatment.id }
          expect(response.status).to eq 200
        end

        it 'destroys the requested treatment' do
          expect {
            delete :destroy, { id: treatment.id }
          }.to change(Treatment, :count).by(-1)
        end
      end

      context 'as a receiver' do
        before(:each) { login treatment.patient }

        it 'tries to delete a treatment' do
          delete :destroy, { id: treatment.id }
          expect(response.status).to eq 200
        end

        it 'destroyes the requested treatment' do
          expect {
            delete :destroy, { id: treatment.id }
          }.to change(Treatment, :count).by(-1)
        end
      end
    end
  end

  describe 'POST submit_treatments' do
    let!(:treatment_template) { create :treatment_template,
                                treatment_type: 'medical_injection' }

    let :valid_params do
      {
        items: [
          {
            'id': treatment_template.id,
            'receiver_id': treatment.patient
          }
        ]
      }
    end

    let(:invalid_params) do
      {
        items: [
          {
            'id': treatment_template.id,
            'receiver_id': 45
          }
        ]
      }
    end

    context 'without sign in' do

      it 'tries to submit the treatments with invalid params' do
        post :submit_treatments, invalid_params
        expect(response.status).to eq 401
      end

      it 'tries to submit the treatments with valid params' do
        post :submit_treatments, valid_params
        expect(response.status).to eq 401
      end
    end

    context 'signed in' do
      context 'as a doctor' do
        before(:each) { login doctor }

        context 'with invalid params' do
          it 'tries to submit the treatments' do
            post :submit_treatments, invalid_params
            expect(response.status).to eq 422
          end

          it "doesn't create the treatments" do
            expect {
              post :submit_treatments, invalid_params
            }.not_to change(Treatment, :count)
          end
        end

        context 'with valid params' do
          it 'tries to submit the treatments' do
            post :submit_treatments, valid_params
            expect(response.status).to eq 200
          end

          it 'creates the treatments' do
            expect {
              post :submit_treatments, valid_params
            }.to change(Treatment, :count).by(1)
          end
        end
      end

      context 'as a receiver' do
        before(:each) { login treatment.patient }

        context 'with invalid params' do
          it 'tries to submit the treatments' do
            post :submit_treatments, invalid_params
            expect(response.status).to eq 422
          end

          it "doesn't create the treatments" do
            expect {
              post :submit_treatments, invalid_params
            }.not_to change(Treatment, :count)
          end
        end

        context 'with valid params' do
          it 'tries to submit the treatments' do
            post :submit_treatments, valid_params
            expect(response.status).to eq 200
          end

          it 'creates the treatments' do
            expect {
              post :submit_treatments, valid_params
            }.to change(Treatment, :count).by(1)
          end
        end
      end
    end
  end

  describe 'PUT prescribe_requested_treatment' do
    let(:valid_params) do
      {
        name: "New name",
        id: treatment.id
      }
    end
    let(:invalid_params) { valid_params.merge({ name: "" }) }

    context 'without sign in' do
      it 'tries to prescribe the treatment with invalid params' do
        put :prescribe_requested_treatment, invalid_params
        expect(response.status).to eq 401
      end

      it 'tries to prescribe the treatment with valid params' do
        put :prescribe_requested_treatment, valid_params
        expect(response.status).to eq 401
      end
    end

    context 'with signed in as a doctor' do
      before(:each) { login doctor }
      context 'with valid params' do
        it 'tries to prescribe the treatment' do
          put :prescribe_requested_treatment, valid_params
          expect(response.status).to eq 200
        end

        it 'updates the treatment' do
          expect {
            put :prescribe_requested_treatment, valid_params
            treatment.reload
          }.to change(treatment, :name)
        end
      end

      context 'with invalid params' do
        it 'tries to prescribe the treatment' do
          put :prescribe_requested_treatment, invalid_params
          expect(response.status).to eq 422
        end

        it 'updates the treatment' do
          expect {
            put :prescribe_requested_treatment, invalid_params
            treatment.reload
          }.not_to change(treatment, :name)
        end
      end
    end
  end

  describe 'GET show' do
    let(:params) { { id: treatment.id } }

    context 'without sign in' do
      it 'tries to get the treatment' do
        get :show, params
        expect(response.status).to eq 401
      end
    end

    context 'with sign in do' do
      it 'tries to get the treatment' do
        login doctor
        get :show, params
        expect(json['item']['id']).to eq treatment.id
      end
    end
  end

  describe 'PUT update_current' do
    let :valid_params do
      {
        'id': treatment.id,
        'doctor_comment': 'updated doctor comment',
        'data': {
          'unit': 'mg tabs',
          'schedule': {
            'bedtime': '10'
          }
        }
      }
    end

    let(:invalid_params) { valid_params.merge(name: '') }

    context 'without sign in' do

      it 'tries to update the current treatment with invalid params' do
        put :update_current, invalid_params
        expect(response.status).to eq 401
      end

      it 'tries to update the current treatment with valid params' do
        put :update_current, valid_params
        expect(response.status).to eq 401
      end
    end

    context 'signed in' do
      context 'as a doctor' do
        before(:each) { login doctor }

        context 'with invalid params' do
          it 'tries to update the current treatment' do
            put :update_current, invalid_params
            expect(response.status).to eq 422
          end

          it "doesn't update the current treatment" do
            expect {
              put :update_current, invalid_params
              treatment.reload
            }.not_to change(treatment, :doctor_comment)
          end
        end

        context 'with valid params' do
          it 'tries to update the current treatment' do
            put :update_current, valid_params
            expect(response.status).to eq 200
          end

          it 'updates the current treatment' do
            expect {
              put :update_current, valid_params
              treatment.reload
            }.to change(treatment, :doctor_comment)
          end
        end
      end

      context 'as a receiver' do
        before(:each) { login treatment.patient }

        it 'tries to update the current treatment with invalid params' do
          put :update_current, invalid_params
          expect(response.status).to eq 422
        end

        context 'with valid params' do
          it 'tries to update the current treatment' do
            put :update_current, valid_params
            expect(response.status).to eq 200
          end

          it 'updates the current treatment' do
            expect {
              put :update_current, valid_params
              treatment.reload
            }.to change(treatment, :doctor_comment)
          end
        end
      end
    end
  end
end
