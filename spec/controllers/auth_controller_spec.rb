require "rails_helper"

RSpec.describe AuthController, type: :controller do
  let(:access_token) { create :access_token }
  let(:user) { create(:patient, PHN: 1111111111,
                      phn_validated: true,
                      tokens: [access_token]) }

  describe 'POST update_password' do
    let(:token_type) { "invite" }
    let(:fake_access_token) { create(:access_token) }
    let(:headers) { { "CONTENT_TYPE" => "application/json" } }

    describe 'request with valid value' do
      context 'phn is validated' do
        let!(:token) { create(:invite_token, user: user) }
        let(:valid_request_params) do
          {
            "token_type" => token_type,
            "token" => token.token,
            "password" => "1111"
          }
        end

        before(:each) do
          allow(User).to receive(:find_by_token).and_return(user)
          allow(user).to receive(:prepare_for_json) { {} }
          allow(user).to receive(:generate_token!) { fake_access_token }

          post :update_password, valid_request_params, headers
        end

        it 'status 200' do
          expect(response).to have_http_status(200)
        end

        it 'content_type json' do
          expect(response.content_type).to eq("application/json")
        end

        it 'response valid params' do
          expect(json["item"]["token"]).to eq fake_access_token.token
        end
      end
    end

    context 'request with invalid value' do
      context 'phn is not validated' do
        let(:user) { create(:patient, PHN: 1111111111) }
        let!(:token) { create(:invite_token, user: user) }
        let(:forbidden) { I18n.t('error.forbidden') }
        let(:valid_request_params) do
          {
            "token_type" => token_type,
            "token" => token.token,
            "password" => "1111"
          }
        end

        before(:each) { post :update_password, valid_request_params, headers }

        it 'status 403' do
          expect(response).to have_http_status(403)
        end

        it 'content_type json' do
          expect(response.content_type).to eq("application/json")
        end

        it 'response valid params' do
          expect(json["message"]).to eq forbidden
        end
      end

      context "password is't set" do
        let!(:token) { create(:invite_token, user: user) }
        let(:message) { 'Please, set new password' }
        let(:invalid_request_params) do
          {
            "token_type" => token_type,
            "token" => token.token,
            "password" => ""
          }
        end

        before(:each) { post :update_password, invalid_request_params, headers }

        it 'status 422' do
          expect(response).to have_http_status(422)
        end

        it 'content_type json' do
          expect(response.content_type).to eq("application/json")
        end

        it 'response valid params' do
          expect(json["message"]).to eq message
        end
      end

      context "user does not exist" do
        let!(:token) { SecureRandom.hex(32) }
        let(:not_found) { I18n.t('error.not_found') }
        let(:valid_request_params) do
          {
            "token_type" => token_type,
            "token" => token,
            "password" => "1111"
          }
        end

        before(:each) { post :update_password, valid_request_params, headers }

        it 'status 404' do
          expect(response).to have_http_status(404)
        end

        it 'content_type json' do
          expect(response.content_type).to eq("application/json")
        end

        it 'response valid params' do
          expect(json["message"]).to eq not_found
        end
      end
    end
  end

  describe 'POST send_reset_password_link' do
    let :valid_params do
      {
        email: user.email
      }
    end

    let(:invalid_params) { valid_params.merge(email: user.email + '1') }

    it 'tries to reset password with invalid_params' do
      post :send_reset_password_link, invalid_params
      expect(response.status).to eq 404
    end

    it 'tries to reset password with valid params' do
      post :send_reset_password_link, valid_params
      expect(response.status).to eq 200
      expect(json['message']).to eq 'ok'
    end
  end

  describe 'POST login' do
    let :valid_params do
      {
        email: user.email,
        password: user.password
      }
    end

    let(:invalid_params) { valid_params.merge(password: user.password * 2) }

    it 'tries to log in with invalid params do' do
      post :login, invalid_params
      expect(response.status).to eq 401
    end

    context 'with valid params' do
      it 'tries to log in' do
        post :login, valid_params
        expect(response.status).to eq 200
      end

      it 'halts 403 when the user is removed from the program' do
        user.update(role: "no role", active: false)
        post :login, valid_params
        expect(response.status).to eq 403
      end
    end

  end

  describe 'POST logout' do

  end

  describe 'GET profile' do
    context 'without log in' do
      it 'tries to get profile info' do
        get :profile
        expect(response.status).to eq 401
      end
    end

    context 'with log in' do
      before(:each) { login user }

      it 'tries to get profile info' do
        get :profile
        expect(response.status).to eq 200
        expect(json['item']['id']).to eq user.id
      end
    end
  end

  describe 'GET token/:token_type/:token' do
    let :valid_params do
      {
        token: user.tokens.first.token,
        token_type: user.tokens.first.token_type
      }
    end

    let(:invalid_params) { valid_params.merge(token: user.tokens.first.token * 2)}

    context 'without sign in' do
      it 'tries to get user by token' do
        get :find_by_token, valid_params
        expect(response.status)
      end
    end

    context 'with sign in' do
      it 'tries to get user by token with invalid params' do
        get :find_by_token, invalid_params
        expect(response.status).to eq 404
      end

      it 'tries to get user by token with valid params' do
        get :find_by_token, valid_params
        expect(response.status).to eq 200
        expect(json['item']['id']).to eq user.id
      end
    end
  end

  describe 'PUT profile' do
    let :valid_params do
      {
        email: 'new@email.com',
        role: 'doctor'
      }
    end

    let(:invalid_params) { valid_params.merge(email: nil) }

    context 'without sign in' do
      it 'tries to submit the profile' do
        put :profile_submit, valid_params, format: :json
        expect(response.status).to eq 401
      end
    end

    context 'with sign in' do
      before(:each) { login user }

      it 'tries to submit the profile with invalid params' do
        put :profile_submit, invalid_params, format: :json
        expect(response.status).to eq 422
      end

      it 'tries to submit the profile with invalid params' do
        put :profile_submit, valid_params, format: :json
        expect(response.status).to eq 200
        expect(json['item']['id']).to eq user.id
      end
    end
  end
end
