require "rails_helper"
RSpec.describe MessagesController, type: :controller do
  let(:patient) { create(:patient) }
  let(:doctor) { create(:doctor) }
  let(:message) { create :message, sender: patient }
  let(:case_manager) { create(:case_manager, id: 3) }
  before :each do
    faye_client = double(:faye_client)
    allow(Faye::Client).to receive(:new).and_return faye_client
    allow(faye_client).to receive(:add_extension)
    allow(faye_client).to receive(:publish)
  end

  describe 'GET index' do
    let!(:patient) { create(:patient, id: 1) }
    let!(:doctor) { create(:doctor, id: 2) }
    let!(:messages) do
      create(:message, id: 1, sender: patient, receiver: doctor)
      create(:message, id: 2, sender: patient, receiver: doctor)
      create(:message, id: 3, sender: doctor, receiver: patient)
      create(:message, id: 4, sender: patient, receiver: case_manager)
      create(:message, id: 5, sender: doctor, receiver: case_manager)
    end
    #unauthorized user
    context 'without sign in' do
      it 'tries to get messages with valid params' do
        get :index, dialog_with: patient.id, order: 'id desc', per_page: 500
        expect(response.status).to eq 401
      end
    end

    #patient
    context 'signed in as patient' do
      before(:each) { login patient } #id 1

      it 'tries to get messages without interlocutor id' do
        get :index, order: 'id desc', per_page: 500

        expect(response.status).to eq 200
        expect(json['collection'].length).to eq(0)
      end

      it 'tries to get messages with himself' do
        get :index, order: 'id desc', dialog_with: 1, per_page: 500

        expect(response.status).to eq 200
        expect(json['collection'].length).to eq(0)
      end

      it 'tries to get messages with valid params' do
        get :index, order: 'id desc', dialog_with: doctor.id, per_page: 500

        expect(response.status).to eq 200
        expect(json['collection'].length).to eq(3)
      end

    end

    # Doctor can talk with anybody (doctors, case-managers, patients)
    context 'signed in as doctor' do
      before(:each) { login doctor } #id 2

      it 'tries to get conversation with patient' do
        get :index, dialog_with: patient.id, order: 'id desc', per_page: 500
        expect(response.status).to eq 200
        expect(json['collection'].length).to eq(3)
      end
    end

    # Should case-manager have an ability to talk with everyone?
    context 'signed in as case_manager' do
      before(:each) { login case_manager } #id 3

      it 'tries to get conversation with patient' do
        get :index, dialog_with: patient.id, order: 'id desc', per_page: 500

        expect(response.status).to eq 200
        expect(json['collection'].length).to eq(1)
      end
    end

  end

  describe 'GET create' do
    let :valid_params do
      {
        receiver_id: doctor.id,
        text: 'a message text',
        channel: 'a channel'
      }
    end
    let(:invalid_params) { valid_params.merge(text: '') }

    context 'without sign in' do
      it 'tries to with invalid params' do
        get :create, invalid_params
        expect(response.status).to eq 401
      end

      it 'tries to with valid params' do
        get :create, valid_params
        expect(response.status).to eq 401
      end
    end

    context 'with sign in' do
      context 'as a patient' do
        before(:each) { login patient }

        context 'with invalid params' do
          it 'tries to create a message' do
            get :create, invalid_params
            expect(response.status).to eq 422
          end

          it "doesn't create a new message" do
            expect {
              get :create, invalid_params
            }.not_to change(Message, :count)
          end
        end

        context 'with valid params' do
          it 'tries to create a message' do
            get :create, invalid_params
            expect(response.status).to eq 422
          end

          it 'creates a message' do
            expect {
              get :create, valid_params
            }.to change(Message, :count)
          end

          it 'publishes the message with faye' do
            faye_client = double(:faye_client)
            allow(Faye::Client).to receive(:new).and_return faye_client
            allow(faye_client).to receive(:add_extension)
            expect(faye_client).to receive(:publish).with(any_args)
            get :create, valid_params
          end
        end
      end

      context 'as a doctor' do
        let :valid_params do
          {
            receiver_id: patient.id,
            text: 'a message text',
            channel: 'a channel'
          }
        end
        before(:each) { login doctor }

        context 'with invalid params' do
          it 'tries to create a message' do
            get :create, invalid_params
            expect(response.status).to eq 422
          end

          it "doesn't create a new message" do
            expect {
              get :create, invalid_params
            }.not_to change(Message, :count)
          end
        end

        context 'with valid params' do
          it 'tries to create a message' do
            get :create, invalid_params
            expect(response.status).to eq 422
          end

          it 'creates a message' do
            expect {
              get :create, valid_params
            }.to change(Message, :count)
          end

          it 'publishes the message with faye' do
            faye_client = double(:faye_client)
            allow(Faye::Client).to receive(:new).and_return faye_client
            allow(faye_client).to receive(:add_extension)
            expect(faye_client).to receive(:publish).with(any_args)
            get :create, valid_params
          end
        end
      end
    end
  end

  describe 'PUT read_multiple' do
    let!(:messages) do
      create(:message, id: 1, sender: patient, receiver: doctor)
      create(:message, id: 2, sender: patient, receiver: doctor)
      create(:message, id: 3, sender: doctor, receiver: patient)
      create(:message, id: 4, sender: patient, receiver: case_manager)
      create(:message, id: 5, sender: doctor, receiver: case_manager)
      create(:message, id: 6, sender: patient, receiver: doctor)
    end
    let(:message_ids) { Message.limit(2).pluck(:id) }
    let(:params) { { message_ids: message_ids } }

    context 'mark messages as read' do
      context 'with login' do
        context 'as a patient' do
          before(:each) { login patient } #id 1

          it 'tries to mark patient messages as read' do
            get :read_multiple, { message_ids: message_ids }
            expect(response.status).to eq 200
            expect(json['updated_item_ids']).to eq(message_ids.map(&:to_s))
            expect(json['dialogs_unread_count']).to eq(1)
          end

          it 'publishes the message with faye' do
            faye_client = double(:faye_client)
            allow(Faye::Client).to receive(:new).and_return faye_client
            allow(faye_client).to receive(:add_extension)
            expect(faye_client).to receive(:publish).exactly(2).times
            get :read_multiple, params
          end
        end

        context 'as a doctor' do
          before(:each) { login doctor } #id 1

          it 'tries to mark doctor messages as read' do
            get :read_multiple, { message_ids: message_ids }
            expect(response.status).to eq 200
            expect(json['updated_item_ids']).to eq(message_ids.map(&:to_s))
            expect(json['dialogs_unread_count']).to eq(1)
          end

          it 'publishes the message with faye' do
            faye_client = double(:faye_client)
            allow(Faye::Client).to receive(:new).and_return faye_client
            allow(faye_client).to receive(:add_extension)
            expect(faye_client).to receive(:publish).exactly(2).times
            get :read_multiple, params
          end
        end
      end
    end
  end
end
