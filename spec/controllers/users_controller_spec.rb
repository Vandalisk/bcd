require "rails_helper"

RSpec.describe UsersController, type: :controller do
  let(:patient) { create :patient }
  let(:doctor) { create :doctor }

  describe 'GET index' do
    context 'without login' do
      it 'tries to get the messages list' do
        get :index
        expect(response.status).to eq 401
      end
    end

    context 'with login' do
      context 'as a patient' do
        it 'tries to get the users list' do
          login patient
          get :index
          user_ids = json['collection'].map { |item| item['id'] }.sort
          expect(response.status).to eq 200
          expect(user_ids).to eq User.pluck(:id)
        end
      end

      context 'as a doctor' do
        it 'tries to get the users list' do
          login doctor
          get :index
          user_ids = json['collection'].map { |item| item['id'] }.sort
          expect(response.status).to eq 200
          expect(user_ids).to eq User.pluck(:id)
        end
      end
    end
  end

  describe 'GET create' do
    let :valid_params do
      {
        email: 'user@gmail.com',
        first_name: 'User',
        last_name: 'Test',
        birthday: Date.today,
        gender: 'male',
        role: 'doctor'
      }
    end
    let(:invalid_params) { valid_params.merge(email: '') }

    context 'without login' do
      context 'with valid_params' do
        it 'tries to create a user with invalid params' do
          get :create, invalid_params
          expect(response.status).to eq 401
        end

        it 'tries to create a user with valid params' do
          get :create, valid_params
          expect(response.status).to eq 401
        end
      end
    end

    context 'with login' do
      context 'as a patient' do
        before(:each) { login patient }

        context 'with invalid params' do
          it 'tries to create a user' do
            get :create, invalid_params
            expect(response.status).to eq 422
          end

          it "doesn't create a user" do
            expect {
              get :create, invalid_params
            }.not_to change(User, :count)
          end
        end

        context 'with valid params' do
          it 'tries to create a user' do
            get :create, valid_params
            expect(response.status).to eq 201
          end

          it 'creates a user' do
            expect {
              get :create, valid_params
            }.to change(User, :count)
          end
        end
      end

      context 'as a doctor' do
        before(:each) { login doctor }

        context 'with invalid params' do
          it 'tries to create a user' do
            get :create, invalid_params
            expect(response.status).to eq 422
          end

          it "doesn't create a user" do
            expect {
              get :create, invalid_params
            }.not_to change(User, :count)
          end
        end

        context 'with valid params' do
          it 'tries to create a user' do
            get :create, valid_params
            expect(response.status).to eq 201
          end

          it 'creates a user' do
            expect {
              get :create, valid_params
            }.to change(User, :count)
          end
        end
      end
    end
  end

  describe 'GET show' do
    let(:target_patient) { create :patient }
    let(:params) { { id: target_patient.id } }

    context 'without login' do
      it 'tries to get the user' do
        get :show, params
        expect(response.status).to eq 401
      end
    end

    context 'with login' do
      context 'as a patient' do
        it 'tries to get the user' do
          login patient
          get :show, params
          expect(response.status).to eq 403
        end
      end

      context 'as a doctor' do
        it 'tries to get the user' do
          login doctor
          get :show, params
          expect(response.status).to eq 200
          expect(json['item']['id']).to eq target_patient.id
        end
      end
    end
  end

  describe 'PUT update' do
    let(:routing_key) { 'dpd.user.update' }

    describe 'ToDPD::Notifier check' do
      let(:doctor) { create(:doctor) }
      let!(:user) { create(:patient) }
      let(:headers) { { "CONTENT_TYPE" => "application/json" } }

      before(:each) { login doctor }

      context 'dpd.user.update' do
        let(:valid_params) { { id: user.id, phone: FFaker::PhoneNumber.short_phone_number } }
        let(:invalid_params) { { id: user.id, first_name: nil } }

        context 'valid_params' do
          it 'routing_key correct' do
            notifier = double(:notifier)
            allow(notifier).to receive(:notify)
            expect(ToDPD::Notifier).to receive(:new).with(routing_key).and_return(notifier)
            put :update, valid_params, headers
          end

          it 'notify performs' do
            notifier = double(:notifier)
            allow(ToDPD::Notifier).to receive(:new).and_return(notifier)
            expect(notifier).to receive(:notify)
            put :update, valid_params, headers
          end
        end

        context 'invalid_params' do
          it "shouldn't perform notify" do
            expect(ToDPD::Notifier).not_to receive(:new)
            put :update, invalid_params, headers
          end
          it '422 code' do
            put :update, invalid_params, headers
            expect(response).to have_http_status(422)
          end
        end
      end
    end

    describe 'Database update' do
      let(:new_email) { patient.email + 'start' }
      let :valid_params do
        {
          id: patient.id,
          email: new_email
        }
      end
      let(:invalid_params) { valid_params.merge(email: '') }

      context 'without sign in' do
        it 'tries to update a user' do
          put :update, valid_params
          expect(response.status).to eq 401
        end
      end

      context 'with sign in' do
        context 'as a patient' do
          before(:each) { login patient }
          it 'tries to update a user' do
            put :update, valid_params
            expect(response.status).to eq 403
          end
        end

        context 'as a doctor' do
          before(:each) { login doctor }

          context 'with invalid params' do
            it 'tries to update the user' do
              put :update, invalid_params
              expect(response.status).to eq 422
            end

            it "doesn't update the user" do
              expect {
                put :update, invalid_params
              }.not_to change(patient, :email)
            end
          end

          context 'with valid params' do
            it 'tries to update the user' do
              notifier = double(:notifier)
              allow(notifier).to receive(:notify)
              allow(ToDPD::Notifier).to receive(:new).with(routing_key).and_return(notifier)

              put :update, valid_params
              expect(response.status).to eq 200
            end
          end
        end
      end
    end
  end

  describe 'GET get_total_scores' do
    let(:second_patient) { create :patient }
    let(:params) { { id: patient.id } }
    let(:second_patient_params) { { id: second_patient.id } }

    context 'without login' do
      it 'tries to get the total scores' do
        get :get_total_scores, params
        expect(response.status).to eq 401
      end
    end

    context 'with login' do
      context 'as a patient' do
        before(:each) { login patient }

        context 'as a receiver' do
          it 'tries to get the total scores if a patient' do
            get :get_total_scores, params
            expect(response.status).to eq 200
            expect(json['scores']['total_score']).to eq patient.total_score
            expect(json['scores']['current_score']).to eq patient.current_score
          end
        end

        context 'as another patient' do
          it 'tries to get the total scores of another patient' do
            get :get_total_scores, second_patient_params
            expect(response.status).to eq 403
          end
        end
      end

      context 'as a doctor' do
        before(:each) { login doctor }

        it 'tries to get the total scores' do
          get :get_total_scores, params
          expect(response.status).to eq 200
          expect(json['scores']['total_score']).to eq patient.total_score
          expect(json['scores']['current_score']).to eq patient.current_score
        end
      end
    end
  end

  describe 'calculate_completed_tasks' do
    let(:second_patient) { create :patient }
    let(:params) { { id: patient.id } }
    let(:second_patient_params) { { id: second_patient.id } }

    context 'without login' do
      it 'tries to calculate the completed tasks' do
        get :calculate_completed_tasks, params
        expect(response.status).to eq 401
      end
    end

    context 'with login' do
      context 'as a patient' do
        before(:each) { login patient }

        context 'as a receiver' do
          it 'tries to calculate completed tasks' do
            get :calculate_completed_tasks, params
            expect(response.status).to eq 200
            expect(json['tasks']).not_to be_empty
          end
        end

        context 'as another user' do
          it "tries to calculate another user's completed tasks" do
            get :calculate_completed_tasks, second_patient_params
            expect(response.status).to eq 403
          end
        end
      end

      context 'as a doctor' do
        before(:each) { login doctor }
        it "tries to calcuate a patient's completed tasks" do
          get :calculate_completed_tasks, params
          expect(response.status).to eq 200
          expect(json['tasks']).not_to be_empty
        end
      end
    end
  end

  describe 'GET check_phn' do
    let(:user) { create(:patient, PHN: 1111111111) }
    let(:request_valid_phn) { { phn: user.PHN, id: user.id } }
    let(:request_invalid_phn) { { phn: 0000000000, id: user.id } }
    let(:response_valid_phn) { { "response" => true, "message" => '' } }
    let(:response_invalid_phn) { { "response" => false, "message" => I18n.t('error.phn_dismatch') } }
    let(:headers) { { "CONTENT_TYPE" => "application/json" } }

    describe 'request with valid phn' do
      before(:each) { get :check_phn, request_valid_phn, headers }

      it 'status 200' do
        expect(response).to have_http_status(200)
      end

      it 'content_type json' do
        expect(response.content_type).to eq("application/json")
      end

      it 'response valid phn' do
        expect(json).to eq(response_valid_phn)
      end
    end

    describe 'request with invalid phn' do
      before(:each) { get :check_phn, request_invalid_phn, headers }

      it 'status 200' do
        expect(response).to have_http_status(200)
      end

      it 'content_type json' do
        expect(response.content_type).to eq("application/json")
      end

      it 'response valid phn' do
        expect(json).to eq(response_invalid_phn)
      end
    end
  end
end
