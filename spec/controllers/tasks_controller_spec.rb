require "rails_helper"
RSpec.describe TasksController, type: :controller do
  let(:patient) { create :patient }
  let(:doctor) { create :doctor }
  let(:treatment) { create :treatment, patient: patient, frequency: :daily }
  let(:task) { create :task, comment: 'An old comment', treatment: treatment }

  describe 'GET index' do
    let!(:requested_treatment) { create :treatment, status: 'requested', period: 1 }
    let!(:new_treatment) { create :treatment, status: 'new', period: 1 }
    let!(:task) { create :task, treatment: requested_treatment, patient: patient }

    let :params do
      {
        treatment_status: 'requested',
        treatment_receiver_id: patient.id
      }
    end

    context 'without sign in' do
      it 'tries to get the tasks list' do
        get :index, params
        expect(response.status).to eq 401
      end
    end

    context 'with sign in' do
      context 'as a doctor' do
        it 'tries to get the tasks list' do
          login doctor
          get :index, params
          task_ids = json['tasks'].map { |task| task['id'] }
          treatment_ids = json['treatments'].map { |treatment| treatment['id'] }

          expect(response.status).to eq 200
          expect(task_ids).to eq [task.id]
          expect(treatment_ids).to eq [requested_treatment.id]
        end
      end

      context 'as a receiver' do
        let!(:another_requested_treatment) { create :treatment, status: 'requested' }

        it 'tries to get the tasks list' do
          login patient
          get :index, params
          expect(response.status).to eq 200

          task_ids = json['tasks'].map { |task| task['id'] }
          treatment_ids = json['treatments'].map { |treatment| treatment['id'] }

          expect(task_ids).to eq [task.id]
          expect(treatment_ids).to eq [requested_treatment.id]
        end
      end
    end
  end

  describe 'PUT update' do
    let :valid_params do
      {
        id: task.id,
        comment: 'New comment'
      }
    end

    context 'without sign in' do
      it 'tries to update the task' do
        put :update, valid_params
        expect(response.status).to eq 401
      end
    end

    context 'with sign in' do
      context 'as a doctor' do
        before(:each) { login doctor }

        context 'with valid params' do
          it 'tries to update the task' do
            put :update, valid_params
            expect(response.status).to eq 200
          end

          it 'updates the task' do
            expect {
              put :update, valid_params
              task.reload
            }.to change(task, :comment)
          end
        end
      end

      context 'as a receiver' do
        before(:each) { login task.patient }

        context 'with valid params' do
          it 'tries to update the task' do
            put :update, valid_params
            expect(response.status).to eq 200
          end

          it 'updates the task' do
            expect {
              put :update, valid_params
              task.reload
            }.to change(task, :comment)
          end
        end
      end
    end
  end

  describe 'PUT update_all' do
    let(:second_task) { create :task, patient: patient, treatment: treatment }
    let :valid_params do
      {
        collection: [
          {
            id: task.id,
            data_completed: 'not nil'
          },
          {
            id: second_task.id,
            data_completed: 'not nil too'
          }
        ]
      }
    end

    context 'without sign in' do
      it 'tries to update several tasks' do
        put :update_all, valid_params
        expect(response.status).to eq 401
      end
    end

    context 'with sign in' do
      context 'as a doctor' do
        before(:each) { login doctor }

        it 'tries to update several tasks' do
          put :update_all, valid_params
          expect(response.status).to eq 200
        end

        it 'updates several tasks' do
          put :update_all, valid_params
          tasks = [task, second_task].map(&:reload)
          data_completed = tasks.map(&:data_completed)
          expect(data_completed).to eq(["not nil", "not nil too"])
        end
      end

      context 'as a receiver' do
        before(:each) { login patient }

        it 'tries to update several tasks' do
          put :update_all, valid_params
          expect(response.status).to eq 200
        end

        it 'updates several tasks' do
          put :update_all, valid_params
          tasks = [task, second_task].map(&:reload)
          data_completed = tasks.map(&:data_completed)
          expect(data_completed).to eq(["not nil", "not nil too"])
        end
      end
    end
  end
end
