require 'rails_helper'

RSpec.describe DialogsController, type: :controller do
  let(:patient) { create :patient }
  let(:doctor) { create :doctor }
  let!(:message) { create :message, receiver: patient, sender: doctor }

  describe 'GET index' do
    let(:second_doctor) { create :doctor }
    let!(:second_message) { create :message, receiver: patient, sender: second_doctor }
    let :params do
      {
        self_id: patient.id
      }
    end
    context 'without sign in' do
      it 'tries to get the dialogs list' do
        get :index, params
        expect(response.status).to eq 401
      end
    end

    context 'with sign in' do
      context 'as a patient' do
        before(:each) { login patient }

        it 'tries to get the dialogs' do
          get :index, params
          dialogs_ids = json['dialogs'].map{ |dialog| dialog['id'] }
          messages_ids = json['messages'].map { |message| message['id'] }
          messages = [message, second_message]
          expect(response.status).to eq 200
          expect(dialogs_ids).to eq [second_doctor, doctor].map(&:id)
          expect(messages_ids).to eq(messages.map(&:id))
        end
      end

      context 'as a doctor' do
        let!(:message) { create :message, receiver: doctor, sender: patient }
        let!(:second_message) { create :message, receiver: doctor, sender: second_doctor }

        before(:each) { login doctor }

        it 'tries to get the dialogs' do
          get :index, params
          dialogs_ids = json['dialogs'].map{ |dialog| dialog['id'] }
          messages_ids = json['messages'].map { |message| message['id'] }
          messages = [message, second_message]
          expect(response.status).to eq 200
          expect(dialogs_ids).to eq [second_doctor, patient].map(&:id)
          expect(messages_ids).to eq(messages.map(&:id))
        end
      end
    end
  end

  describe 'GET dialogs_unread_count' do
    let :params do
      {
        id: patient.id
      }
    end

    context 'without login' do
      it 'tries to get the number of unread dialogs' do
        get :dialogs_unread_count, params
        expect(response.status).to eq 401
      end
    end

    context 'with login' do
      context 'as a patient' do
        let!(:message) { create :message, receiver: patient }

        before(:each) { login patient }

        it 'tries to get the unread dialogs number' do
          get :dialogs_unread_count, params
          expect(response.status).to eq 200
          expect(json['dialogs_unread_count']).to eq 1
        end
      end

      context 'as a doctor' do
        let!(:message) { create :message, receiver: doctor }

        before(:each) { login doctor }

        it 'tries to get the unread dialogs number' do
          get :dialogs_unread_count, params
          expect(response.status).to eq 200
          expect(json['dialogs_unread_count']).to eq 1
        end
      end
    end
  end
end
