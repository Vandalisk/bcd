require "rails_helper"

RSpec.describe AccountsController, type: :controller do
  let(:doctor) { create :doctor }
  let(:admin) { create :admin }
  let(:patient) { create :patient }
  let(:case_manager) { create :case_manager}

  describe 'GET patients' do
    let!(:patients) { 3.times { create :patient } }

    context 'without sign in' do
      it 'tries to get patients' do
        get :patients
        expect(response.status).to eq 401
      end
    end

    context 'with sign in' do
      context 'as a patient' do
        before(:each) { login patient }

        it 'tries to get patients' do
          get :patients
          expect(response.status).to eq 403
        end
      end

      context 'as a doctor' do
        before(:each) { login doctor }

        it 'tries to get patients' do
          get :patients
          patients_ids = json.map { |patient| patient['id'] }
          expect(patients_ids).to eq(User.patients.active.includes(:sent_treatments).pluck(:id))
        end
      end

      context 'as a case manager' do
        before(:each) { login case_manager }

        it 'tries to get patients' do
          get :patients
          patients_ids = json.map { |patient| patient['id'] }
          expect(patients_ids).to eq(User.patients.active.includes(:sent_treatments).pluck(:id))
        end
      end

      context 'as an admin' do
        before(:each) { login admin }

        it 'tries to get patients' do
          get :patients
          patients_ids = json.map { |patient| patient['id'] }
          expect(patients_ids).to eq(User.patients.active.includes(:sent_treatments).pluck(:id))
        end
      end
    end
  end

  describe 'GET staff' do
    let!(:staff) { 3.times { create :doctor } }

    context 'without sign in' do
      it 'tries to get the staff' do
        get :staff
        expect(response.status).to eq 401
      end
    end

    context 'with sign in' do
      context 'as a patient' do
        before(:each) { login patient }

        it 'tries to get the staff' do
          get :staff
          expect(response.status).to eq 403
        end
      end

      context 'as a doctor' do
        before(:each) { login doctor }

        it 'tries to get the staff' do
          get :staff
          staff_ids = json.map { |staff| staff['id'] }
          expect(staff_ids).to eq(User.staff.except_current(doctor.id).pluck(:id))
        end
      end

      context 'as a case manager' do
        before(:each) { login case_manager }

        it 'tries to get the staff' do
          get :staff
          staff_ids = json.map { |staff| staff['id'] }
          expect(staff_ids).to eq(User.staff.except_current(case_manager.id).pluck(:id))
        end
      end

      context 'as an admin' do
        before(:each) { login admin }

        it 'tries to get the staff' do
          get :staff
          staff_ids = json.map { |staff| staff['id'] }
          expect(staff_ids).to eq(User.staff.except_current(admin.id).pluck(:id))
        end
      end
    end
  end
end
