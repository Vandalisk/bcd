module JsonHelpers
  def json
    response.body.empty? ? "" : JSON.parse(response.body)
  end
end
