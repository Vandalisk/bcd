module LoginHelper
  def login user
    token = user.tokens.where(token_type: 'access').first || user.generate_token!('access')
    request.headers["Authorization"] = "Token token=#{token.token}"
  end
end
