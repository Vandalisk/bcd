class BunnyMocker

  def initialize(attributes)
    @exchange = attributes[:type]
    @exchange_name = attributes[:name]
    @routing_key = attributes[:routing_key]
  end

  def response
    connection = Bunny.new
    connection.start
    channel = connection.create_channel
    exchange = channel.public_send(@exchange, @exchange_name)
    result = nil
    channel.queue(@routing_key).bind(exchange, routing_key: @routing_key).subscribe do |delivery_info, metadata, payload|
      result = payload
    end
    connection.close
    result
  end
end
