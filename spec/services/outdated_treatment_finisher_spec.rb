require 'rails_helper'

RSpec.describe OutdatedTreatmentsFinisher do
  context 'Should move active treatments with date_end in past into "completed" status every day' do
    let(:treatment_template) { create(:treatment_template) }
    let!(:current_treatment) { create(:treatment, treatment_template: treatment_template) }
    let!(:outdated_treatment) { create(:treatment, :outdated, treatment_template: treatment_template) }

    before(:all){
        Treatment.skip_callback(:save, :after, :to_current_callback)
        Treatment.skip_callback(:save, :after, :set_full_name)
        Treatment.skip_callback(:save, :after, :save_event_to_feed)
        Treatment.skip_callback(:update, :after, :prescription_to_DPD)
        Treatment.skip_callback(:update, :after, :edition_to_DPD)
        Treatment.skip_callback(:update, :after, :removal_to_DPD)
      }

    context "finish!" do
      it "should decrease count of outdated treatments" do
        expect{subject.class.finish!}.to change(Treatment.outdated, :count).from(1).to(0)
      end

      it "should decrease count of active treatments" do
        expect{subject.class.finish!}.to change(Treatment.active, :count).from(2).to(1)
      end
    end
  end
end
