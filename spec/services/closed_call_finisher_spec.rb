require 'rails_helper'

RSpec.describe ClosedCallFinisher do
  describe '.finish!' do
    context 'with outdated dialog' do
      let!(:room) { create :room, last_used: Time.now - 5.minutes }

      it 'deletes outdated dialogs' do
        expect do
          ClosedCallFinisher.finish!
        end.to change(TalkingStick::Room, :count)
      end
    end

    context 'with new dialog' do
      let!(:room) { create :room, last_used: Time.now }

      it "doesn't delete new dialogs" do
        expect do
          ClosedCallFinisher.finish!
        end.not_to change(TalkingStick::Room, :count)
      end
    end
  end
end
