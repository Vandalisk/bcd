require 'rails_helper'

describe "PowerPointCalculator" do
  context 'should calculate points with known power, frequency and quantity' do
    it "power 1 daily quantity 10 to eq 11" do
      expect(PowerPointCalculator.calculate(1, "daily", 10)).to eq 10
    end

    it "power 2 every_other_day quantity 20 to eq 44" do
      expect(PowerPointCalculator.calculate(2, "every_other_day", 20)).to eq 80
    end

    it "power 3 2_days_per_week quantity 30 to eq 96" do
      expect(PowerPointCalculator.calculate(3, "2_days_per_week", 30)).to eq 360
    end

    it "power 4 3_days_per_week quantity 40 to eq 168" do
      expect(PowerPointCalculator.calculate(4, "3_days_per_week", 40)).to eq 320
    end

    it "power 5 4_days_per_week quantity 50 to eq 260" do
      expect(PowerPointCalculator.calculate(5, "4_days_per_week", 50)).to eq 500
    end

    it "power 6 5_days_per_week quantity 60 to eq 366" do
      expect(PowerPointCalculator.calculate(6, "5_days_per_week", 60)).to eq 360
    end

    it "power 7 6_days_per_week quantity 70 to eq 497" do
      expect(PowerPointCalculator.calculate(7, "6_days_per_week", 70)).to eq 490
    end

    it "power 8 weekly quantity 80 to eq 704" do
      expect(PowerPointCalculator.calculate(8, "weekly", 80)).to eq 5120
    end

    it "power 9 twice_monthly quantity 90 to eq 945" do
      expect(PowerPointCalculator.calculate(9, "twice_monthly", 90)).to eq 12150
    end

    it "power 10 twice_monthly quantity 100 to eq 1200" do
      expect(PowerPointCalculator.calculate(10, "monthly", 100)).to eq 30000
    end
  end
end
