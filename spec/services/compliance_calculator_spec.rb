require 'rails_helper'

RSpec.describe ComplianceCalculator do
  let!(:treatment) { create :treatment }
  let! :tasks do
    2.times { |i| create :task, treatment: treatment,
                                is_completed: true,
                                date: Date.yesterday }
    3.times { |i| create :task, treatment: treatment,
                                is_completed: false,
                                date: Date.yesterday }
  end

  describe '.calculate' do
    it 'changes compliance rate' do
      treatment.update(compliance_rate: '-1')
      expect do
        ComplianceCalculator.calculate!
        treatment.reload
      end.to change(treatment, :compliance_rate)
    end
  end

  describe '.calculate_compliance_for' do
    it 'calcualtes the compilance rate' do
      expect(ComplianceCalculator.calculate_compliance_for(treatment)).to eq 40
    end
  end
end
