FactoryGirl.define do
  factory :room, class: TalkingStick::Room do |room|
    room.name { FFaker::Name.first_name }
    room.last_used Time.now
  end
end
