FactoryGirl.define do
  factory :message do
    text FFaker::Lorem.paragraph
    association :sender, factory: :doctor
    association :receiver, factory: :patient

    trait :from_patient do
      association :sender, factory: :patient
    end

    trait :from_case_manager do
      association :sender, factory: :case_manager
    end

    trait :to_doctor do
      association :receiver, factory: :doctor
    end

    trait :to_case_manager do
      association :receiver, factory: :case_manager
    end
  end
end
