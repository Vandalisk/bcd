# TODO: refactor with traits
FactoryGirl.define do
  factory :patient, class: User do
    email      { FFaker::Internet.email }
    role       'patient'
    password   '1111'
    first_name { FFaker::Name.first_name }
    last_name  { FFaker::Name.last_name }
    phone      { FFaker::PhoneNumber.short_phone_number }
    birthday   { Time.at(rand(60.years.ago.to_i..20.years.ago.to_i)).to_date }
    gender     { ['male','female'][rand(2)] }
  end

  factory :doctor, class: User do
    email      { FFaker::Internet.email }
    role       'doctor'
    password   '1111'
    first_name { FFaker::Name.first_name }
    last_name  { FFaker::Name.last_name }
    phone      { FFaker::PhoneNumber.short_phone_number }
    birthday   { Time.at(rand(60.years.ago.to_i..20.years.ago.to_i)).to_date }
    gender     { ['male', 'female'][rand(2)] }
  end

  factory :case_manager, class: User do
    email      { FFaker::Internet.email }
    role       'case_manager'
    password   '1111'
    first_name { FFaker::Name.first_name }
    last_name  { FFaker::Name.last_name }
    phone      { FFaker::PhoneNumber.short_phone_number }
    birthday   { Time.at(rand(60.years.ago.to_i..20.years.ago.to_i)).to_date }
    gender     { ['male', 'female'][rand(2)] }
  end

  factory :admin, class: User do
    email      { FFaker::Internet.email }
    role       'admin'
    password   '1111'
    first_name { FFaker::Name.first_name }
    last_name  { FFaker::Name.last_name }
    phone      { FFaker::PhoneNumber.short_phone_number }
    birthday   { Time.at(rand(60.years.ago.to_i..20.years.ago.to_i)).to_date }
    gender     { ['male', 'female'][rand(2)] }
  end
end
