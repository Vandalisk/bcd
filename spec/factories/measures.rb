FactoryGirl.define do
  factory :measure do |object|
    object.when Measure::MEASURE_WHEN_TYPES.sample
    object.value Faker::Number.decimal(2)
    object.date Date.today
  end
end
