FactoryGirl.define do
  factory :token do
    association :user, factory: :patient
    token { SecureRandom.hex(32) }

    trait :invite do
      token_type 'invite'
    end

    trait :access do
      token_type 'access'
    end

    factory :invite_token, traits: [:invite]
    factory :access_token, traits: [:access]
  end
end
