FactoryGirl.define do
  factory :treatment do
    doctor
    patient
    treatment_template
    treatment_type { treatment_template.treatment_type }
    name { treatment_template.name }
    data { treatment_template.data }
    description { FFaker::Lorem.paragraph }
    frequency 'daily'
    period { [2, 7][rand(2)] }
    status 'active'

    trait :outdated do
      after(:create) do |treatment|
        treatment.date_end = 4.days.ago
        treatment.save
      end
    end
  end
end
