FactoryGirl.define do
  factory :task do
    is_completed false
    date { Date.today + rand(50) }
    comment FFaker::Lorem.paragraph
    treatment
  end
end
