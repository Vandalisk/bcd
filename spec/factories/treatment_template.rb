FactoryGirl.define do
  factory :data_hash, class:Hash do
    unit TreatmentBase::TREATMENT_UNITS.map{|m| m[:key]}.sample
    dosage Random.rand(1000)
    schedule {{
      'breakfast': Random.rand(10),
      'lunch': Random.rand(10),
      'dinner': Random.rand(10),
      'bedtime': Random.rand(10)
    }}

    initialize_with { attributes }
  end


  factory :treatment_template do
    association :author, factory: :doctor
    sequence(:name){ |n| "treatment_temlate #{n}"}
    data FactoryGirl.build(:data_hash)
    treatment_type TreatmentBase::TREATMENT_TYPES.sample
    frequency TreatmentBase::FREQUENCY_OPTIONS.map{ |m| m[:key] }.sample
    period Random.rand(10)
    power Random.rand(1..10)
    insurance { ['yes', 'no', 'partial', 'yes_with_authority'].sample }
    description FFaker::Lorem.paragraph
  end
end
