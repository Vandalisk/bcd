require "rails_helper"

RSpec.describe 'BCD program invitation scenario', type: :request do
  let(:user) { create(:patient, PHN: 1111111111) }
  let(:request_valid_phn) { { phn: user.PHN, id: user.id } }
  let(:response_valid_phn) { { "response" => true, "message" => '' } }
  let!(:token) { create(:invite_token, user: user) }
  let(:fake_access_token) { create(:access_token) }
  let(:valid_request_params) do
    {
      "token_type" => 'invite',
      "token" => token.token,
      "password" => "1111"
    }
  end

  it "should update patient's password and allow access to program" do
    get "/users/#{user.id}/check_phn", request_valid_phn
    expect(json).to eq(response_valid_phn)

    user.reload

    allow(User).to receive(:find_by_token).and_return(user)
    allow(user).to receive(:prepare_for_json) { {} }
    allow(user).to receive(:generate_token!) { fake_access_token }

    post '/auth/update_password', valid_request_params
    expect(json["item"]["token"]).to eq fake_access_token.token
  end
end
